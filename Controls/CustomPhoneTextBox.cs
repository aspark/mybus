﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace myBus.Controls
{
    public class CustomPhoneTextBox : PhoneTextBox
    {
        public CustomPhoneTextBox()
        {
            
        }

        public bool DisableKeyboard { get; set; }

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            if (!DisableKeyboard)
            {
                base.OnGotFocus(e);
            }
        }

        protected override void OnHold(System.Windows.Input.GestureEventArgs e)
        {
            if (!DisableKeyboard)
            {
                base.OnHold(e);
            }
        }

        //over
    }
}
