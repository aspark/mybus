﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace myBus.Controls
{
    public class CustomAutoCompleteBox : AutoCompleteBox
    {
        public CustomAutoCompleteBox()
        {
            DefaultStyleKey = typeof(CustomAutoCompleteBox);
        }

        protected override void OnGotFocus(System.Windows.RoutedEventArgs e)
        {
            var ctrl = FocusManager.GetFocusedElement();
            if (ctrl is TextBox)
                (ctrl as TextBox).SelectAll();
            base.OnGotFocus(e);
        }

        public bool IsWaitForIME { get; set; }

        public string Hint
        {
            get { return (string)GetValue(HintProperty); }
            set { SetValue(HintProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Hint.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HintProperty =
            DependencyProperty.Register("Hint", typeof(string ), typeof(CustomAutoCompleteBox), new PropertyMetadata(""));

        private bool _canRetrive = true;
        protected override void OnKeyDown(System.Windows.Input.KeyEventArgs e)
        {
            if (IsWaitForIME && Language.IetfLanguageTag.Contains("zh"))
                _canRetrive = false;

            base.OnKeyDown(e);

        }

        protected override void OnTextChanged(System.Windows.RoutedEventArgs e)
        {
            if (IsWaitForIME)
            {
                if (_canRetrive)
                    base.OnPopulating(new PopulatingEventArgs(Text));
                _canRetrive = true;
            }

            base.OnTextChanged(e);
        }
        
        protected override void OnPopulating(PopulatingEventArgs e)
        {
            if (!IsWaitForIME)
            {
                base.OnPopulating(e);
            }
        }
    }
}
