﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace myBus.Controls
{
    public class CustomButton : Button
    {
        public CustomButton()
        {
            DefaultStyleKey = typeof(CustomButton);
        }

        public override void OnApplyTemplate()
        {

            base.OnApplyTemplate();
        }

        protected override void OnContentChanged(object oldContent, object newContent)
        {
            if (newContent == null || string.IsNullOrEmpty(newContent.ToString()))
            {
                VisualStateManager.GoToState(this, "Hint", false);
            }
            else
            {
                VisualStateManager.GoToState(this, "HideHint", false);
            }
            base.OnContentChanged(oldContent, newContent);
        }

        public string Hint
        {
            get { return (string)GetValue(HintProperty); }
            set { SetValue(HintProperty, value); }
        }

        public static readonly DependencyProperty HintProperty =
            DependencyProperty.Register("Hint", typeof(string), typeof(CustomButton), new PropertyMetadata("暂无"));



        public SolidColorBrush HintForeground
        {
            get { return (SolidColorBrush)GetValue(HintForegroundProperty); }
            set { SetValue(HintForegroundProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HintForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HintForegroundProperty =
            DependencyProperty.Register("HintForeground", typeof(SolidColorBrush), typeof(CustomButton), new PropertyMetadata(new SolidColorBrush(Colors.Gray)));

        
    }
}
