﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Controls
{
    class CustomPhoneApplicationPage : PhoneApplicationPage
    {
        public CustomPhoneApplicationPage()
        {
            DefaultStyleKey = typeof(CustomPhoneApplicationPage);
        }
    }
}
