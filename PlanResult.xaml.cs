﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using myBus.ViewModel;
using System.Windows.Media.Animation;

namespace myBus
{
    public partial class PlanResult : PhoneApplicationPage
    {
        PlanResultViewModel _viewModel = null;
        public PlanResult()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (_viewModel == null)
            {
                _viewModel = new PlanResultViewModel();
                this.FillPropertyFromQuery(_viewModel);
                this.DataContext = _viewModel;
                _viewModel.IsShowHintChanged += (obj, args) =>
                {
                    var stb = this.Resources["sbHintMoveDown"] as Storyboard;
                    if (stb != null)
                    {
                        if (_viewModel.IsShowHint)
                            stb.Begin();
                        else
                            stb.Stop();
                    }
                };
                _viewModel.CalcPlan();

                //add command bar : locator in map
            }

            base.OnNavigatedTo(e);
        }
    }
}