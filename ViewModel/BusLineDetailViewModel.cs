﻿using Microsoft.Phone.Shell;
using Microsoft.Xna.Framework.Audio;
using myBus.Biz;
using myBus.Common;
using myBus.Model;
using myBus.Model.KuMiKe;
using myBus.Server;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Windows.Devices.Geolocation;

namespace myBus.ViewModel
{
    class BusLineDetailPageViewModel : ViewModelBase, IDisposable
    {
        BusManager _busManager = BusManager.Instance;

        [FillFromQuery]
        public string SubLineName { get; set; }

        private int __currentSubLineID_ = 0;
        [FillFromQuery]
        public int SubLineID {
            set {
                __currentSubLineID_ = value;
            }
            get {
                if (_currentSubline != null)
                {
                    return _currentSubline.id;
                }
                return __currentSubLineID_;
            }
        }

        [FillFromQuery]
        public double Latitude { get; set; }

        [FillFromQuery]
        public double Longitude { get; set; }

        private Timer _refreshTimer = null;
        private int _refreshTimerInterval = 300;
        public BusLineDetailPageViewModel()
        {
            IsOpen = true;
            StationDistance = 58;
            Stations = new ObservableCollection<BusStationViewModel>();
            NearBusList = new ObservableCollection<BusViewModel>();
            RefreshCommand = new NormalCommand(RefreshState);
            SwitchDirectionCommand = new NormalCommand(SwitchSubline);
            SwitchStationCommand = new NormalCommand(SwitchCurrentStation);
            TrackingViewModel = new TrackingViewModel();
            if (AppSettings.Current.CanAutoRefresh)
            {
                _RefreshRemainMilliseconds = AppSettings.Current.AutoRefreshInterval * 1000;
                CanAutoRefresh = true;
            }
        }

        public double StationDistance { get; set; }

        public ObservableCollection<BusStationViewModel> Stations
        {
            get { return base.GetValue<ObservableCollection<BusStationViewModel>>("Stations"); }
            private set { base.SetValue("Stations", value); }
        }

        public ObservableCollection<BusViewModel> NearBusList
        {
            get { return base.GetValue<ObservableCollection<BusViewModel>>("NearBusList"); }
            private set { base.SetValue("NearBusList", value); }
        }

        public string TerminateStation
        {
            get { return base.GetValue<string>("TerminateStation"); }
            set { base.SetValue("TerminateStation", value); }
        }

        public string ServiceTime
        {
            get { return base.GetValue<string>("ServiceTime"); }
            set { base.SetValue("ServiceTime", value); }
        }

        public string Price
        {
            get { return base.GetValue<string>("Price"); }
            set { base.SetValue("Price", value); }
        }

        public BusStationViewModel CurrentStation
        {
            get { return base.GetValue<BusStationViewModel>("CurrentStation"); }
            set { base.SetValue("CurrentStation", value); }
        }

        public bool IsOpen
        {
            get { return base.GetValue<bool>("IsOpen"); }
            set { base.SetValue("IsOpen", value); }
        }

        public string NearBusPrompt
        {
            get { return base.GetValue<string>("NearBusPrompt"); }
            set { base.SetValue("NearBusPrompt", value); }
        }

        private int __refreshRemainMilliseconds = int.MaxValue;
        private int _RefreshRemainMilliseconds
        {
            get { return __refreshRemainMilliseconds; }
            set
            {
                __refreshRemainMilliseconds = value;
                if (__refreshRemainMilliseconds > 0)
                {
                    var ts = new TimeSpan(0, 0, 0, 0, __refreshRemainMilliseconds);
                    if (ts.Minutes == 0)
                    {
                        RefreshTimerPrompt = string.Format("{0}秒后刷新", ts.Seconds);
                    }
                    else
                    {
                        if (ts.Seconds == 0)
                        {
                            RefreshTimerPrompt = string.Format("{0}分钟后刷新", ts.Minutes);
                        }
                        else
                        {
                            RefreshTimerPrompt = string.Format("{0}分{1}秒后刷新", ts.Minutes, ts.Seconds);
                        }
                    }
                }
                else
                {
                    RefreshTimerPrompt = "正在刷新";
                }
            }
        }

        public bool CanAutoRefresh
        {
            get { return base.GetValue<bool>("IsAutoRefresh"); }
            set
            {
                if (value)
                {
                    if (_refreshTimer == null)
                    {
                        _refreshTimer = new Timer(s =>
                        {
                            Deployment.Current.Dispatcher.BeginInvoke(() =>
                            {
                                if (_RefreshRemainMilliseconds <= 0)
                                {
                                    _RefreshRemainMilliseconds = AppSettings.Current.AutoRefreshInterval * 1000;
                                    RefreshState(null);
                                }
                                else
                                {
                                    _RefreshRemainMilliseconds -= _refreshTimerInterval;
                                }
                            });
                        }, null, 300, _refreshTimerInterval);
                    }
                }
                else {
                    if (_refreshTimer != null)
                    {
                        _refreshTimer.Dispose();
                        _refreshTimer = null;
                    }
                }
                base.SetValue("IsAutoRefresh", value);
            }
        }

        public string RefreshTimerPrompt
        {
            get { return base.GetValue<string>("RefreshTimerPrompt"); }
            set { base.SetValue("RefreshTimerPrompt", value); }
        }

        public ICommand RefreshCommand { get; set; }

        public ICommand SwitchDirectionCommand { get; set; }

        public ICommand SwitchStationCommand { get; set; }

        public TrackingViewModel TrackingViewModel { get; private set; }

        private void SwitchCurrentStation(object parameters)
        {
            if (parameters is BusStationViewModel)
            {
                var stationVM = parameters as BusStationViewModel;
                if (stationVM.IsCurrent)
                    return;

                if (CurrentStation != null) 
                    CurrentStation.IsCurrent = false;
                stationVM.IsCurrent = true;
                CurrentStation = stationVM;
                ChangeStationsStatus(); 
                RefreshState(parameters);
            }
        }

        private void SwitchSubline(object parameters)
        {
            if (_sublines != null)
            {
                if (_currentSubline == null)
                    _currentSubline = _sublines.FirstOrDefault();
                else
                {
                    _currentSubline = _sublines.FirstOrDefault(s => s.id != _currentSubline.id);
                }
                CurrentStation = null;
                RefreshState(parameters);
            }
        }

        //private BusStationViewModel _currentStation = null;
        private SubLineInfoDTO _currentSubline = null;
        private List<SubLineInfoDTO> _sublines = null;

        public async void RefreshState(object parameters)
        {
            IsLoading = true;

            if (CurrentStation != null && _currentSubline != null)
            {
                var ret = await _busManager.GetNearBus(_currentSubline.id, CurrentStation.id, NearBusList == null ? null : NearBusList.Select(b => b.ID).ToList());
                if (ret != null)
                {
                    FillNearBusList(CurrentStation, ret);
                }
            }
            else
            {
                NearBusExtraInfoDTO ret = null;
                if (SubLineID > 0)
                {
                    ret = await _busManager.GetNearBus(SubLineID, Latitude, Longitude);
                }
                else
                    ret = await _busManager.GetNearBus(SubLineName, Latitude, Longitude);
                

                if (ret != null)
                {
                    IsOpen = false;
                    if (ret.dir != null)
                    {
                        _sublines = ret.dir;
                        _currentSubline = _sublines.FirstOrDefault(d => d.id == ret.line);
                        if (_currentSubline == null) _currentSubline = _sublines.FirstOrDefault();

                        Stations.Clear();
                        TerminateStation = "";
                        if (ret.lineinfo != null)
                            IsOpen = ret.lineinfo.isopen == EnumIsOPEN.TRUE;

                        if (_currentSubline != null)
                        {
                            TerminateStation = _currentSubline.end_station;
                            ServiceTime = _currentSubline.service_time;
                            if (_currentSubline.stations != null)
                            {
                                _currentSubline.stations.ForEach(s =>
                                {
                                    Stations.Add(new BusStationViewModel
                                    {
                                        id = s.id,
                                        name = s.name,
                                        distance = s.distance,
                                        seq = s.seq,
                                        HasPassed = false,
                                        IsCurrent = ret.station != null && ret.station.id == s.id,
                                        Latitude = s.lat,
                                        Longitude = s.lng
                                    });
                                });
                            }
                        }
                    }

                    var currentStation = ChangeStationsStatus();

                    FillNearBusList(currentStation, ret.cars);
                }
                else
                {
                    Helper.DisplayPrompt("没有获取到该线路的数据");
                }
            }
            IsLoading = false;
        }

        private BusStationViewModel ChangeStationsStatus()
        {
            var curStation = Stations.FirstOrDefault(s => s.IsCurrent);

            foreach (var station in Stations.Where(s => s.HasPassed))
            {
                station.HasPassed = false;
            }

            foreach (var station in Stations)
            {
                if (!station.IsCurrent)
                    station.HasPassed = true;
                else
                    break;
            }

            CurrentStation = curStation;
            //if (!(curStation != null && CurrentStation != null && CurrentStation.id == curStation.id))
            //{
            OnCurrentStationChanged(new CurrentStationChangedEventArgs(CurrentStation));
            //}

            Price = "";
            foreach (var station in Stations)
            {
                station.Price = "";
            }
            if (_currentSubline != null && !string.IsNullOrWhiteSpace(_currentSubline.price))
            {
                if (_currentSubline.price.IndexOf('-') > 0 || _currentSubline.price.Length > 1 || string.Compare(_currentSubline.price, "2") > 0)
                {
                    Price = _currentSubline.price;
                    var currentIndex = Stations.IndexOf(CurrentStation);
                    PriceManager.Instance.GetPrice(SubLineName, CurrentStation.name, _currentSubline.end_station, currentIndex).ContinueWith(ret =>
                    {
                        if (currentIndex > -1)
                        {
                            var movedIndex = Stations.Count - 1 - currentIndex;
                            foreach (var key in ret.Result.Keys.Reverse())
                            {
                                while (true)
                                {
                                    if (movedIndex >= key && currentIndex + movedIndex > -1)
                                    {
                                        Stations[currentIndex + movedIndex].Price = ret.Result[key].ToString();
                                    }
                                    else
                                        break;

                                    movedIndex--;
                                }
                            }
                        }
                    }, TaskScheduler.FromCurrentSynchronizationContext());
                }
                else
                {
                    Price = _currentSubline.price;
                }
            }

            return CurrentStation;
        }

        private void FillNearBusList(BusStationViewModel currentStation, List<BusInfoDTO> busList)
        {
            if (IsOpen)
            {
                DateTime startServiceTime = DateTime.Now;
                DateTime endSerivceTime = DateTime.Now;
                if (_currentSubline != null)
                {
                    Helper.GetServiceTime(_currentSubline.service_time, out startServiceTime, out endSerivceTime);
                }

                NearBusPrompt = "没有获取到车辆数据";

                NearBusList.Clear();
                if (busList != null)
                {
                    busList.ForEach(c =>
                    {
                        NearBusList.Add(new BusViewModel
                        {
                            ID = c.carid,
                            Name = c.name,
                            CurrentStationID = c.stationid,
                            CurrentState = c.stationstate,
                            WaitingTime = new TimeSpan(0, 0, (int)c.waittime),
                            Position = CalcPosition(currentStation, c),
                            HasPassed = c.waittime >= 20000 || c.distance > 200000
                        });
                    });
                }

                if (NearBusList != null && NearBusList.Count > 0)
                {
                    var nearestBus = NearBusList.Where(b => !b.HasPassed).OrderBy(n => n.WaitingTime).FirstOrDefault();
                    if (nearestBus != null)
                    {
                        int remainStationsCount = 0;
                        if (nearestBus.WaitingTime.TotalSeconds < Helper.ArrivedWaitTime)
                        {
                            NearBusPrompt = string.Format("最近：{0} 已经到达", nearestBus.Name);
                        }
                        else if (nearestBus.WaitingTime.TotalSeconds < Helper.ArrivingWaitTime)
                        {
                            NearBusPrompt = string.Format("最近：{0} 即将到达", nearestBus.Name);
                        }
                        else
                        {
                            NearBusPrompt = string.Format("最近：{0} {1} {2}", nearestBus.Name, GetRemainStationCount(currentStation, nearestBus, out remainStationsCount), Helper.GetRemainTimeString(nearestBus.WaitingTime));
                        }

                        if (AppSettings.Current.NeedWaitingAlarm && AppSettings.Current.CanAutoRefresh)
                        {
                            double threshold = remainStationsCount;
                            if (AppSettings.Current.WaitingAlarmThreshold > 30)//is seconds
                            {
                                threshold = nearestBus.WaitingTime.TotalSeconds;
                            }

                            if (threshold <= AppSettings.Current.WaitingAlarmThreshold)
                            {
                                NotifyCommingBus(nearestBus.ID, string.Format("【{0}】{1}快要到站了", string.Join(" ", SubLineName.ToArray()), nearestBus.Name));
                            }
                        }

                    }
                    else
                    {
                        NearBusPrompt = Helper.GetBusServicePrompt(startServiceTime, endSerivceTime, "尚未发车");
                    }

                    foreach (var bus in NearBusList)
                    {
                        if (bus.HasPassed)
                        {
                            bus.Prompt = "";//过站
                        }
                        else if (bus.WaitingTime.TotalSeconds < 30)
                        {
                            bus.Prompt = "";//已经到达
                        }
                        else if (bus.WaitingTime.TotalSeconds < 120)
                        {
                            bus.Prompt = "";//即将到达
                        }
                        else
                        {
                            bus.Prompt = string.Format("约{0:f0}分钟", bus.WaitingTime.TotalMinutes);
                        }
                    }
                }
                else
                {
                    NearBusPrompt = Helper.GetBusServicePrompt(startServiceTime, endSerivceTime, "尚未发车");
                }

            }
        }

        private string GetRemainStationCount(BusStationViewModel currentStation, BusViewModel bus, out int remainCount)
        {
            remainCount = 0;
            if (_currentSubline != null)
            {
                var carStationIndex = _currentSubline.stations.FindIndex(s => s.id == bus.CurrentStationID);
                if (carStationIndex > -1)
                {
                    var currentStationIndex = _currentSubline.stations.FindIndex(s => s.id == currentStation.id);
                    if (bus.CurrentState == EnumStationState.Leave)
                        carStationIndex++;

                    var remainStationsCount = currentStationIndex - carStationIndex;
                    if (remainStationsCount > -1)
                    {
                        remainCount = remainStationsCount;
                        if (remainStationsCount > 0)
                            return string.Format("约{0}站", remainStationsCount);
                    }
                }
            }

            return "";
        }

        private double CalcPosition(BusStationViewModel currentStation, BusInfoDTO bus)
        {
            double position = 0;
            if (currentStation != null && _currentSubline != null)
            {
                if (_currentSubline != null)
                {
                    //var currentStationIndex = _currentSubline.stations.FindIndex(s => s.id == currentStation.id);
                    //if (currentStationIndex > -1)
                    //{
                    //    var carStationIndex = _currentSubline.stations.FindIndex(s => s.id == bus.stationid);
                    //    if (carStationIndex > -1)
                    //    {
                    //        position = carStationIndex * StationDistance + StationDistance / 2;
                    //        double distance = 0;
                    //        if (bus.waittime >= 20000 || bus.distance > 200000)
                    //        {
                    //            //if(NearBusList!
                    //            distance = 200;//已经过站了
                    //        }
                    //        else
                    //        {
                    //            distance = _currentSubline.stations[currentStationIndex].distance - bus.distance - _currentSubline.stations[carStationIndex].distance;
                    //        }

                    //        position += StationDistance / Math.Abs(_currentSubline.stations[carStationIndex + 1].distance - _currentSubline.stations[carStationIndex].distance) * distance;
                    //    }
                    //    else
                    //    {
                    //        //belowe
                    //    }
                    //}

                    double distance = currentStation.distance - bus.distance;
                    if (distance < 0 || bus.waittime >= 20000 || bus.distance > 200000)
                    {
                        //if(NearBusList!
                        distance = currentStation.distance + 200;//已经过站了
                    }

                    if (Stations != null && Stations.Count > 0)
                    {
                        BusStationViewModel lastStation = null;
                        foreach (var station in Stations)
                        {
                            if (station.distance <= distance)
                            {
                                position += StationDistance;
                            }
                            else
                            {
                                var lastDistance = lastStation == null ? 0 : lastStation.distance;
                                position += (distance - lastDistance) / (station.distance - lastDistance) * StationDistance - StationDistance / 2;
                                break;
                            }
                            lastStation = station;
                        }
                    }

                }

            }

            return position;
        }

        public event EventHandler<CurrentStationChangedEventArgs> CurrentStationChanged;

        private void OnCurrentStationChanged(CurrentStationChangedEventArgs e)
        {
            if (CurrentStationChanged != null)
            {
                CurrentStationChanged(this, e);
            }
        }

        private Dictionary<int, string> NotifiedBusList = new Dictionary<int, string>();
        private void NotifyCommingBus(int busID, string content)
        {
            if (!NotifiedBusList.ContainsKey(busID))
            {
                NotifiedBusList.Add(busID, content);

                var title = "候车提醒";
                Helper.PlaySound("Assets/bus.wav", 2).ContinueWith(t => {
                    Helper.Speech(content);
                });

                if (App.RunningInBackground)
                {
                    ShellToast toast = new ShellToast();
                    toast.Title = title;
                    toast.Content = content;
                    toast.Show();
                }
                else
                {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        MessageBox.Show(content, title, MessageBoxButton.OK);
                    });
                }
            }
        }

        public void Dispose()
        {
            if (_refreshTimer != null)
            {
                _refreshTimer.Dispose();
            }

            TrackingViewModel.StopTrackLocation();
        }
    }

    class CurrentStationChangedEventArgs : EventArgs
    {
        public BusStationViewModel CurrenItem { get; private set; }

        public CurrentStationChangedEventArgs(BusStationViewModel item)
        {
            CurrenItem = item;
        }
    }

    class BusStationViewModel : ViewModelBase
    {
        public int distance { get; set; }

        public int id { get; set; }

        public string name { get; set; }

        public int seq { get; set; }

        //public int waittime { get; set; }

        //public int LineID { get; set; }
        
        public bool HasPassed
        {
            get { return base.GetValue<bool>("HasPassed"); }
            set { base.SetValue("HasPassed", value); }
        }

        public bool IsCurrent
        {
            get { return base.GetValue<bool>("IsCurrent"); }
            set { base.SetValue("IsCurrent", value); }
        }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string Price
        {
            get { return base.GetValue<string>("Price"); }
            set { base.SetValue("Price", value); }
        }

    }

    class BusViewModel : ViewModelBase
    {
        public int ID { get; set; }
        public double Position { get; set; }
        public string Name { get; set; }
        //public BusStationViewModel Station { get; set; }
        //public string Stationstate { get; set; }
        public int CurrentStationID { get; set; }
        public TimeSpan WaitingTime { get; set; }
        public bool HasPassed { get; set; }
        public string Prompt { get; set; }
        public EnumStationState CurrentState { get; set; }
    }

    class TrackingViewModel : ViewModelBase
    {
        public TrackingViewModel()
        {
            TrackLocationCommand = new NormalCommand(TrackLocationImpl);
            IsTrackingLocation = false;
        }

        public ICommand TrackLocationCommand { get; set; }

        public bool IsTrackingLocation
        {
            get { return base.GetValue<bool>("IsTrackingLocation"); }
            set { base.SetValue("IsTrackingLocation", value); }
        }

        public bool CanStopTracking { get; private set; }

        public string TrackingTarget
        {
            get { return base.GetValue<string>("TrackingTarget"); }
            set { base.SetValue("TrackingTarget", value); }
        }

        public string TrackingDistance
        {
            get { return base.GetValue<string>("TrackingDistance"); }
            set { base.SetValue("TrackingDistance", value); }
        }

        private TrackLocationManager _track = null;
        private void TrackLocationImpl(object parameters)
        {
            if (AppSettings.Current.IsEnableGeo)
            {
                if (parameters is BusStationViewModel)
                {
                    var station = parameters as BusStationViewModel;

                    if (_track != null)
                    {
                        StopTrackLocation();
                    }
                    _track = new TrackLocationManager(station.Latitude, station.Longitude);//, AppSettings.Current.ArrivedAlarmThreshold
                    _track.DistanceChanged += _track_DistanceChanged;
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        TrackingTarget = station.name;
                        IsTrackingLocation = true;
                    });

                    TrackingDistance = "正在计算距离...";
                    CanStopTracking = false;
                    _track.Start();
                }
            }
            else
            {
                MessageBox.Show("请在设置中开启定位功能");
            }
        }

        void _track_DistanceChanged(object sender, DistanceChangedEventArgs e)
        {
            if (e.Meters <= AppSettings.Current.ArrivedAlarmThreshold)
            {
                NotifyArrived(string.Format("{0} 就要到了", TrackingTarget));
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    //StopTrackLocation();
                    CanStopTracking = true;
                    TrackingDistance = string.Format("小于{0}米", AppSettings.Current.ArrivedAlarmThreshold);
                });
            }
            else
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    TrackingDistance = e.Meters.ToString("#公里##0米");
                });
            }
        }

        //void _track_Arrived(object sender, EventArgs e)
        //{

        //}

        internal void StopTrackLocation()
        {
            IsTrackingLocation = false;
            if (_track != null)
            {
                _track.DistanceChanged -= _track_DistanceChanged;
                _track.Stop();
                _track = null;
            }
        }

        private void NotifyArrived(string content)
        {
            string title = "到站提醒";

            Helper.PlaySound("Assets/bus.wav", 2).ContinueWith(t =>
            {
                Helper.Speech(content);
            });

            if (App.RunningInBackground)
            {
                ShellToast toast = new ShellToast();
                toast.Title = title;
                toast.Content = content;
                toast.Show();
            }
            else
            {
                Deployment.Current.Dispatcher.BeginInvoke(() => {
                    MessageBox.Show(content, title, MessageBoxButton.OK);
                });
            }
        }

    }
}
