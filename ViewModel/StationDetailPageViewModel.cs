﻿using myBus.Biz;
using myBus.Common;
using myBus.Model;
using myBus.Model.KuMiKe;
using myBus.Server;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace myBus.ViewModel
{
    class StationDetailPageViewModel:ViewModelBase
    {
        protected BusManager _busManager = null;
        public StationDetailPageViewModel()
        {
            _busManager = BusManager.Instance;
            Lines = new ObservableCollection<StationDetailItemViewModel>();
            ClickLineCommand = new NormalCommand(parameters =>
            {
                if (parameters is StationDetailItemViewModel)
                {
                    var item = parameters as StationDetailItemViewModel;
                    var url = UrlHelper.GetUrl("/BusLineDetailPage.xaml", new
                    {
                        SubLineID = item.LineID,
                        SubLineName = item.Name,
                        //sdf = item.StartStationName,
                        StationID = item.CurrentStationID,
                        Latitude = item.CurrentStationLat,
                        Longitude = item.CurrentStationLng
                    }, UriKind.RelativeOrAbsolute);
                    App.RootFrame.Navigate(url);
                }
            });

        }

        [FillFromQuery]
        public string StationName
        {
            get { return base.GetValue<string>("StationName"); }
            set { base.SetValue("StationName", value); }
        }

        [FillFromQuery]
        public double Latitude { get; set; }

        [FillFromQuery]
        public double Longitude { get; set; }

        public bool IsHintEmptyLinesList
        {
            get { return base.GetValue<bool>("IsHintEmptyLinesList"); }
            set { base.SetValue("IsHintEmptyLinesList", value); }
        }

        public ObservableCollection<StationDetailItemViewModel> Lines { get; private set; }

        public async void GetLines()
        {
            IsLoading = true;
            if (!string.IsNullOrWhiteSpace(StationName))
            {
                Lines.Clear();
                try
                {
                    var ret = await _busManager.GetStationDetail(StationName);
                    if (ret != null && ret.data != null)
                    {
                        foreach (var l in ret.data.OrderByDescending(l => l.isopen == EnumIsOPEN.TRUE ? 1 : 0).ThenBy(l => l.line_name))
                        {
                            Lines.Add(new StationDetailItemViewModel
                            {
                                LineID = l.sublineid,
                                Name = l.line_name,
                                StartStationName = l.start_station,
                                EndStationName = l.end_station,
                                ServiceTime = l.begin_time + "-" + l.end_time,
                                CurrentStationID = l.station.id,
                                CurrentStationLat = l.station.lat,
                                CurrentStationLng = l.station.lng,
                                IsOpened = l.isopen == EnumIsOPEN.TRUE,
                                RemainTime = GetRemainTime(l.wait_time, l.begin_time + "-" + l.end_time)
                            });
                        }
                    }
                }
                catch { }

                IsHintEmptyLinesList = Lines == null || Lines.Count == 0;
            }
            IsLoading = false;
        }

        private string GetRemainTime(long waittime, string serviceTime)
        {
            if (waittime >= 0)
            {
                if (waittime > 20000)
                {
                    DateTime startServiceTime;
                    DateTime endSerivceTime;

                    Helper.GetServiceTime(serviceTime, out startServiceTime, out endSerivceTime);
                    return Helper.GetBusServicePrompt(startServiceTime, endSerivceTime, "尚未发车");
                }
                else if (waittime < Helper.ArrivedWaitTime)
                {
                    return "到站";
                }
                else if (waittime < Helper.ArrivingWaitTime)
                {
                    return "即将到站";
                }
                else
                    return Helper.GetRemainTimeString(new TimeSpan(0, 0, (int)waittime));
            }

            return "未知";
        }

        public ICommand ClickLineCommand { get; private set; }
    }

    class StationDetailItemViewModel
    {
        public int LineID { get; set; }

        public string Name { get; set; }

        public string StartStationName { get; set; }

        public string EndStationName { get; set; }

        public string ServiceTime { get; set; }

        public bool IsOpened { get; set; }

        public string RemainTime { get; set; }

        public int CurrentStationID { get; set; }

        public double CurrentStationLat { get; set; }

        public double CurrentStationLng { get; set; }
    }
}
