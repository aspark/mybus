﻿using myBus.Biz;
using myBus.Common;
using myBus.Model.Baidu;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace myBus.ViewModel
{
    class PlanResultViewModel : ViewModelBase
    {
        public PlanResultViewModel()
        {
            PlansViewModel = new PlanResultViewModel_Plans();
            HintViewModel = new PlanResultViewModel_Hint(this);
        }

        [FillFromQuery]
        public bool UseCurrentLocation { get; set; }

        [FillFromQuery]
        public string Origin
        {
            get { return base.GetValue<string>("Origin"); }
            set { base.SetValue("Origin", value); }
        }

        [FillFromQuery]
        public string Destination
        {
            get { return base.GetValue<string>("Destination"); }
            set { base.SetValue("Destination", value); }
        }

        [FillFromQuery]
        public double Latitude { get; set; }

        [FillFromQuery]
        public double Longitude { get; set; }

        public async void CalcPlan()
        {
            LBSResultBase<dynamic> ret = null;
            IsLoading = true;

            if (UseCurrentLocation)
                ret = await LocationManager.Instance.GetPlan(Origin, Destination, Latitude, Longitude);
            else
                ret = await LocationManager.Instance.GetPlan(Origin, Destination);

            if (ret.result is DirectionHintResultDTO)
            {
                FillHint(ret.result as DirectionHintResultDTO);
            }
            else if (ret.result is DirectionResultDTO)
            {
                FillResult(ret.result as DirectionResultDTO);
            }

            IsLoading = false;
        }

        public PlanResultViewModel_Plans PlansViewModel { get; private set; }

        private void FillResult(DirectionResultDTO dto)
        {
            PlansViewModel.Routes.Clear();
            if (dto != null)
            {
                if (dto.routes != null)
                {
                    PlanResultRouteViewModel route = null;
                    PlanStepViewModel step = null;
                    PlanStepViewModel prevStep = null;
                    int index = 1;
                    foreach (var rt in dto.routes)
                    {
                        route = new PlanResultRouteViewModel();
                        route.Name = string.Format("方案{0}", index++);
                        route.TotalDistance = rt.scheme[0].distance;
                        route.WasteMinutes = rt.scheme[0].duration / 60;

                        int stepIndex = 1;
                        foreach (var st in rt.scheme[0].steps)
                        {
                            if (st.Count > 0)
                            {
                                step = new PlanStepViewModel();
                                step.Index = stepIndex++;
                                //step.StepType = st[0].vehicle == null ? EnumStepType.Walk : (EnumStepType)st[0].vehicle.type;
                                step.DefaultVehicleViewModel = ConvertToVehicle(st[0], step);
                                if (step.DefaultVehicleViewModel != null)
                                {
                                    step.DefaultVehicleViewModel.VehicleType = step.StepType;
                                    step.OtherVehiclesChoice = st.Skip(1).Select(s => ConvertToVehicle(s, step)).ToList();
                                    step.Next = null;
                                    if (prevStep != null)
                                    {
                                        prevStep.Next = step;
                                        if (string.IsNullOrWhiteSpace(prevStep.DefaultVehicleViewModel.EndName))
                                        {
                                            prevStep.DefaultVehicleViewModel.EndName = step.DefaultVehicleViewModel.StartName;
                                        }
                                    }
                                    route.Steps.Add(step);

                                    prevStep = step;
                                }
                            }
                        }

                        var first = route.Steps.FirstOrDefault();
                        if (first != null)
                        {
                            first.DefaultVehicleViewModel.StartName = "";//= "起点";
                        }

                        var last = route.Steps.LastOrDefault(s => s.Next == null);
                        if (last != null)
                        {
                            if (string.IsNullOrWhiteSpace(last.DefaultVehicleViewModel.EndName))
                            {
                                last.DefaultVehicleViewModel.EndName = "终点";
                            }
                        }

                        foreach (var walk in route.Steps.Where(s => s.StepType == EnumVehicleType.Walk))
                        {
                            if (walk.DefaultVehicleViewModel.Distance <= 0)
                                walk.DefaultVehicleViewModel.Distance = 10;
                        }

                        route.ChangeVehicleTimes = route.Steps.Count(s => s.StepType != EnumVehicleType.Walk) - 1;
                        if (route.ChangeVehicleTimes < 0) route.ChangeVehicleTimes = 0;
                        route.WalkDistance = route.Steps.Sum(s => s.StepType == EnumVehicleType.Walk ? s.DefaultVehicleViewModel.Distance : 0);

                        PlansViewModel.Routes.Add(route);
                    }
                }
                else
                {
                    //if (App.RootFrame.CanGoBack)
                    //    App.RootFrame.GoBack();
                    Helper.DisplayPrompt("哦，查询不到线路~");
                }
            }
        }

        private VehicleViewModel ConvertToVehicle(StepDTO chooice, PlanStepViewModel parent)
        {
            var vehicle = new VehicleViewModel();
            vehicle.Parent = parent;
            vehicle.Distance = chooice.distance;
            vehicle.EndLocation = chooice.stepDestinationLocation;
            vehicle.StartLocation = chooice.stepOriginLocation;
            vehicle.VehicleType = chooice.vehicle == null ? EnumVehicleType.Walk : (EnumVehicleType)chooice.vehicle.type;
            if (chooice.vehicle != null)
            {
                vehicle.EndName = chooice.vehicle.end_name;
                vehicle.StartName = chooice.vehicle.start_name;
                vehicle.VehicleName = chooice.vehicle.name;
                vehicle.StopCount = chooice.vehicle.stop_num;
                vehicle.ServiceTime = string.Format("{0}-{1}", chooice.vehicle.start_time, chooice.vehicle.end_time);
            }
            else
            {
                vehicle.StartName = chooice.sname;
            }

            return vehicle;
        }

        private void FillHint(DirectionHintResultDTO dto)
        {
            if (dto != null)
            {
                if ((dto.origin == null || dto.origin.Count == 0) && (dto.destination == null || dto.destination.Count == 0))
                {
                    //if (App.RootFrame.CanGoBack)
                    //    App.RootFrame.GoBack();
                    Helper.DisplayPrompt("您输入的地址太模糊啦！");
                    return;
                }

                if (dto.destination != null && dto.destination.Count > 0)
                {
                    HintViewModel.DestinationViewModel.Items.Clear();
                    foreach (var dest in dto.destination)
                    {
                        HintViewModel.DestinationViewModel.Items.Add(new PlaceViewModel { 
                            Name=dest.name,
                            Address=dest.address,
                            Location = dest.location,
                            Parent = HintViewModel.DestinationViewModel
                        });
                    }
                    HintViewModel.DestinationViewModel.IsShowSelector = true;
                }

                if (dto.origin != null && dto.origin.Count > 0)
                {
                    HintViewModel.OriginViewModel.Items.Clear();
                    foreach (var origin in dto.origin)
                    {
                        HintViewModel.OriginViewModel.Items.Add(new PlaceViewModel
                        {
                            Name = origin.name,
                            Address = origin.address,
                            Location = origin.location,
                            Parent = HintViewModel.OriginViewModel
                        });
                    }
                    HintViewModel.OriginViewModel.IsShowSelector = true;
                }

                IsShowHint = true;
            }
        }

        internal void PostHintSelect()
        {
            if (HintViewModel != null && !HintViewModel.DestinationViewModel.IsShowSelector&& !HintViewModel.OriginViewModel.IsShowSelector)
            {
                IsShowHint = false;
                CalcPlan();
            }
        }

        public PlanResultViewModel_Hint HintViewModel { get; private set; }

        public bool IsShowHint
        {
            get { return base.GetValue<bool>("IsShowHint"); }
            set { 
                base.SetValue("IsShowHint", value);
                OnIsShowHintChanged(EventArgs.Empty);
            }
        }

        public event EventHandler<EventArgs> IsShowHintChanged;
        private void OnIsShowHintChanged(EventArgs args)
        {
            if (IsShowHintChanged != null)
                IsShowHintChanged(this, args);
        }
    }

    class PlanResultViewModel_Hint : ViewModelBase
    {
        PlanResultViewModel _parent = null;
        public PlanResultViewModel_Hint()
        {

            OriginViewModel = new PlaceItemsViewModel(new Action<object>(para =>
            {
                var item = para as PlaceViewModel;
                if (item != null)
                {
                    _parent.Origin = item.Name;
                    OriginViewModel.IsShowSelector = false;
                    _parent.PostHintSelect();
                }
            }));

            DestinationViewModel = new PlaceItemsViewModel(new Action<object>(para =>
            {
                var item = para as PlaceViewModel;
                if (item != null)
                {
                    _parent.Destination = item.Name;
                    DestinationViewModel.IsShowSelector = false;
                    _parent.PostHintSelect();
                }
            }));
        }

        public PlanResultViewModel_Hint(PlanResultViewModel parent)
            : this()
        {
            _parent = parent;
        }

        public PlaceItemsViewModel OriginViewModel { get; private set; }

        public PlaceItemsViewModel DestinationViewModel { get; private set; }
    }

    class PlaceItemsViewModel : ViewModelBase
    {
        public Action<object> _selectHandle = null;
        public PlaceItemsViewModel()
        {
            Items = new ObservableCollection<PlaceViewModel>();
            SelectPlaceCommand = new NormalCommand(p =>
            {
                if (_selectHandle != null)
                {
                    _selectHandle(p);
                }
            });
        }

        public PlaceItemsViewModel(Action<object> selectHandle)
            : this()
        {
            _selectHandle = selectHandle;
        }

        public ObservableCollection<PlaceViewModel> Items { get; private set; }

        public bool IsShowSelector
        {
            get { return base.GetValue<bool>("IsShowSelector"); }
            set { base.SetValue("IsShowSelector", value); }
        }

        public ICommand SelectPlaceCommand { get; private set; }
    }

    class PlaceViewModel
    {
        public PlaceItemsViewModel Parent { get; set; }

        public string Name { get; set; }
        public string Address { get; set; }
        public LocationDTO Location { get; set; }
    }

    class PlanResultViewModel_Plans : ViewModelBase
    {
        public PlanResultViewModel_Plans()
        {
            Routes = new ObservableCollection<PlanResultRouteViewModel>();
            ClickStationCommand = new NormalCommand(param => {
                if (param != null && param is VehicleCommandViewModel)
                {
                    VehicleCommandViewModel model = param as VehicleCommandViewModel;
                    if (model.VehicleType == EnumVehicleType.Bus)
                    {
                        if (model.DataType == EnumVehicleCommandDataType.LineName)
                        {
                            var url = UrlHelper.GetUrl("/BusLineDetailPage.xaml", new
                            {
                                SubLineName = model.Data.TrimEnd('路'),
                                Latitude = AppStatus.Current.CurrentLat,
                                Longitude = AppStatus.Current.CurrentLng
                            }, UriKind.RelativeOrAbsolute);
                            App.RootFrame.Navigate(url);
                        }
                        else if (model.DataType == EnumVehicleCommandDataType.StationName)
                        {
                            var url = UrlHelper.GetUrl("/StationDetailPage.xaml", new
                            {
                                StationName = model.Data.TrimEnd('站'),
                                Latitude = AppStatus.Current.CurrentLat,
                                Longitude = AppStatus.Current.CurrentLng
                            }, UriKind.RelativeOrAbsolute);
                            App.RootFrame.Navigate(url);
                        }

                    }
                    else if (model.VehicleType == EnumVehicleType.Metro)
                    {
                        //todo:not supported
                    }
                    else if (model.VehicleType == EnumVehicleType.Walk)
                    {
                        //todo:not supported
                    }
                }
            });

            ShowInMapCommand = new NormalCommand(ShowInMap);
        }

        public ObservableCollection<PlanResultRouteViewModel> Routes
        {
            get { return base.GetValue<ObservableCollection<PlanResultRouteViewModel>>("Routes"); }
            set { base.SetValue("Routes", value); }
        }

        public ICommand ClickStationCommand { get; private set; }
        public ICommand ShowInMapCommand { get; private set; }


        private void ShowInMap(object parameter)
        {
            if (parameter is PlanResultRouteViewModel)
            { 
                MapPage.CurrentRoute = parameter as PlanResultRouteViewModel;
                App.RootFrame.Navigate(new Uri("/MapPage.xaml", UriKind.RelativeOrAbsolute));
            }
        }
    }


    class PlanResultRouteViewModel
    {
        public PlanResultRouteViewModel()
        {
            Steps = new List<PlanStepViewModel>();
        }

        public string Name { get; set; }

        public int WasteMinutes { get; set; }

        private int _changeVehicleTimes;
        public int ChangeVehicleTimes
        {
            get { return _changeVehicleTimes; }
            set { 
                _changeVehicleTimes = value;
                if (_changeVehicleTimes < 0)
                    _changeVehicleTimes = 0;

                if (_changeVehicleTimes <= 0) 
                    ChangeVehicleTimesPrompt = "直达";
                else
                    ChangeVehicleTimesPrompt = string.Format("换乘{0}次", _changeVehicleTimes);
            }
        }

        public string ChangeVehicleTimesPrompt { get; private set; }

        public int WalkDistance { get; set; }

        public long TotalDistance { get; set; }

        public List<PlanStepViewModel> Steps { get; private set; }

        private void ShowMap()
        { 
            
        }
    }

    class PlanStepViewModel
    {
        public PlanStepViewModel()
        {
            OtherVehiclesChoice = new List<VehicleViewModel>();
        }

        public int Index { get; set; }

        public EnumVehicleType StepType
        {
            get
            {
                if (DefaultVehicleViewModel != null)
                {
                    return DefaultVehicleViewModel.VehicleType;
                }

                return EnumVehicleType.Walk;
            }
#if DEBUG
            private set {
                DefaultVehicleViewModel.VehicleType = value;
            }
#endif
        }


        public PlanStepViewModel Next { get; set; }

        public VehicleViewModel DefaultVehicleViewModel { get; set; }

        public List<VehicleViewModel> OtherVehiclesChoice { get; set; }
    }

    class VehicleViewModel
    {
        public PlanStepViewModel Parent { get; set; }

        public string VehicleName { get; set; }

        public EnumVehicleType VehicleType { get; set; }

        public int StopCount { get; set; }

        public int Distance { get; set; }

        public string StartName { get; set; }

        public LocationDTO StartLocation { get; set; }

        public string EndName { get; set; }

        public LocationDTO EndLocation { get; set; }

        public string ServiceTime { get; set; }

        public VehicleCommandViewModel StartNameCommandParameter
        {
            get
            {
                return new VehicleCommandViewModel
                {
                    Data = StartName,
                    DataType = EnumVehicleCommandDataType.StationName,
                    VehicleType = VehicleType
                };
            }
        }

        public VehicleCommandViewModel EndNameCommandParameter
        {
            get
            {
                return new VehicleCommandViewModel
                {
                    Data = EndName,
                    DataType = EnumVehicleCommandDataType.StationName,
                    VehicleType = (VehicleType == EnumVehicleType.Walk && Parent.Next != null && Parent.Next.DefaultVehicleViewModel != null) ? Parent.Next.StepType : VehicleType
                };
            }
        }

        public VehicleCommandViewModel VehicleNameCommandParameter
        {
            get
            {
                return new VehicleCommandViewModel
                {
                    Data = VehicleName,
                    DataType = EnumVehicleCommandDataType.LineName,
                    VehicleType = VehicleType
                };
            }
        }
    
    }

    class VehicleCommandViewModel
    {
        public string Data { get; set; }

        public EnumVehicleType VehicleType { get; set; }

        public EnumVehicleCommandDataType DataType { get; set; }
    }

    enum EnumVehicleType
    {
        Bus = 0,
        Metro = 1,
        Walk = 5
    }

    enum EnumVehicleCommandDataType
    { 
        LineName=0,
        StationName=1
    }
}
