﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.ViewModel
{
    public class DropdownItemViewModel<T>
    {
        public DropdownItemViewModel()
        {

        }

        public DropdownItemViewModel(string text, T value)
        {
            Text = text;
            Value = value;
        }

        public string Text { get; set; }

        public T Value { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is DropdownItemViewModel<T>)
            {
                var target = obj as DropdownItemViewModel<T>;
                return object.Equals(target.Value, this.Value);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Value == null ? 0 : Value.GetHashCode();
        }
    }
}
