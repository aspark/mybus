﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace myBus.ViewModel
{
    abstract class ViewModelBase : INotifyPropertyChanged
    {
         
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            { 
                PropertyChanged(this,new PropertyChangedEventArgs(propertyName));
            }
        }

        private Dictionary<string, object> _properties = new Dictionary<string, object>();

        protected void SetValue<T>(string propertyName, T value)
        {
            T oldValue = GetValue<T>(propertyName);
            _properties[propertyName] = value;
            if (!object.Equals(oldValue, value))
            {
                OnPropertyChanged(propertyName);
            }
        }

        protected T GetValue<T>(string propertyName)
        {
            if (_properties.ContainsKey(propertyName))
                return (T)_properties[propertyName];

            return default(T);
        }

        private int _loadingCount = 0;
        public bool IsLoading 
        {
            get { return GetValue<bool>("IsLoading"); }
            set {
                var newValue = value;

                lock (this)
                {
                    if (newValue)
                        _loadingCount++;
                    else if (_loadingCount > 0)
                        _loadingCount--;

                    if (_loadingCount == 0)
                    {
                        SetValue("IsLoading", false);
                    }
                    else
                        SetValue("IsLoading", true);
                    }
            }
        }
    }

    class NormalCommand : ICommand
    {
        private Action<object> _cmd = null;
        private Func<object, bool> _canExecute = null;

        public NormalCommand(Action<object> cmd)
        {
            _cmd = cmd;
        }

        public NormalCommand(Action<object> cmd, Func<object, bool> canExecute)
            : this(cmd)
        {
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            if (_canExecute != null) return _canExecute(parameter);

            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void OnCanExecuteChanged()
        {
            if (CanExecuteChanged != null) CanExecuteChanged(this, EventArgs.Empty);
        }

        public void Execute(object parameter)
        {
            if (_cmd != null) _cmd(parameter);
        }
    }

}
