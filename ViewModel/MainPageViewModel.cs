﻿using myBus.Biz;
using myBus.Common;
using myBus.Model.Baidu;
using myBus.Model.Common;
using myBus.Server;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Device.Location;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace myBus.ViewModel
{
    class MainPageViewModel : ViewModelBase//, System.ComponentModel.IDataErrorInfo
    {
        public MainPageViewModel()
        {
            BusViewModel = new MainPageViewModel_Bus(this);
            StationViewModel = new MainPageViewModel_Station(this);
            SettingsViewModel = new MainPageViewModel_Settings(this);
            PlanViewModel = new MainPageViewModel_Plan(this);

            FreshNetworkState();

            RetryNetworkCommand = new NormalCommand(parameters =>
            {
                FreshNetworkState();
            });

        }

        public MainPageViewModel_Bus BusViewModel { get; private set; }

        public MainPageViewModel_Station StationViewModel { get; private set; }

        public MainPageViewModel_Settings SettingsViewModel { get; private set; }

        public MainPageViewModel_Plan PlanViewModel { get; private set; }

        public bool IsNetworkEnabled
        {
            get { return base.GetValue<bool>("IsNetworkEnabled"); }
            private set { base.SetValue("IsNetworkEnabled", value); }
        }

        /// <summary>
        /// bd coordinate
        /// </summary>
        public double Latitude
        {
            get { return base.GetValue<double>("Latitude"); }
            private set { base.SetValue("Latitude", value); }
        }

        /// <summary>
        /// bd coordinate
        /// </summary>
        public double Longitude
        {
            get { return base.GetValue<double>("Longitude"); }
            private set { base.SetValue("Longitude", value); }
        }

        public string Position
        {
            get { return base.GetValue<string>("Position"); }
            set { base.SetValue("Position", value); }
        }

        private GeoCoordinateWatcher _geoWatcher = null;
        internal bool HasLocatedPosition { get; set; }
        private void Locate()
        {
            if (_geoWatcher == null && AppSettings.Current.IsEnableGeo)
            {
                _geoWatcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High);
                _geoWatcher.MovementThreshold = 100;

                _geoWatcher.StatusChanged += geoWatcher_StatusChanged;
                _geoWatcher.PositionChanged += geoWatcher_PositionChanged;
                _geoWatcher.Start();
                Position = "正在定位...";
                if (_geoWatcher.Position != null && _geoWatcher.Position.Location != null && !_geoWatcher.Position.Location.IsUnknown)
                {
                    SetLocation(_geoWatcher.Position.Location.Latitude, _geoWatcher.Position.Location.Longitude, true, true);//false decream one request
                }
            }
        }

        void geoWatcher_StatusChanged(object sender, GeoPositionStatusChangedEventArgs e)
        {
            switch (e.Status)
            {
                case GeoPositionStatus.Disabled:
                    Position = "GPS不可用！";
                    break;
                case GeoPositionStatus.Initializing:
                    //Position = "初始化GPS...";
                    break;
                case GeoPositionStatus.NoData:
                    break;
                case GeoPositionStatus.Ready:
                    //_geoWatcher.StatusChanged -= geoWatcher_StatusChanged;
                    //Position = "";
                    //if (_geoWatcher.Position.Location != null)
                    //{
                    //    SetLocation(_geoWatcher.Position.Location.Latitude, _geoWatcher.Position.Location.Longitude, true);
                    //}

                    //_geoWatcher.Dispose();
                    //_geoWatcher = null;
                    break;
                default:
                    break;
            }
        }

        void geoWatcher_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            _geoWatcher.StatusChanged -= geoWatcher_StatusChanged;
            _geoWatcher.PositionChanged -= geoWatcher_PositionChanged;
            SetLocation(e.Position.Location.Latitude, e.Position.Location.Longitude, true, false);
            _geoWatcher.Dispose();
            _geoWatcher = null;
        }

        internal async void SetLocation(double lat, double lng, bool retriveLocation, bool isShowLatLng)
        {
            Latitude = lat;
            Longitude = lng;
            AppStatus.Current.CurrentLat = lat;
            AppStatus.Current.CurrentLng = lng;
            if (isShowLatLng)
                Position = string.Format("当前坐标：{0},{1}", Latitude, Longitude);

            if (IsNetworkEnabled && retriveLocation)
            {
                //Position = "正在获取地理信息...";
                IsLoading = true;
                var ret = await LocationManager.Instance.GetPosition(Latitude, Longitude);
                if (ret != null)
                {
                    LocationManager.Instance.SetCity(ret.addressComponent.city);
                    Position = ret.formatted_address;
                    Latitude = ret.location.lat;
                    Longitude = ret.location.lng;
                    HasLocatedPosition = true;
                }
                else
                {
                    //todo:retry locator

                    Position = "哦定位失败，是来自星星的你吗？";
                }

                IsLoading = false;
            }
        }

        public ICommand RetryNetworkCommand { get; private set; }

        private void FreshNetworkState()
        {
            IsNetworkEnabled = Microsoft.Phone.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
            if (IsNetworkEnabled)
            {
                Locate();
            }
        }

        internal void Dispose()
        {
            Debug.WriteLine("main page dispose");
            if (_geoWatcher != null)
            {
                _geoWatcher.Stop();
                _geoWatcher.Dispose();
            }
        }
    }

    class MainPageViewModel_Bus : ViewModelBase
    {
        BusManager _busManager = BusManager.Instance;
        private MainPageViewModel _parent = null;

        private MainPageViewModel_Bus()
        {
            IsHintEmptyRecentList = true;
            Recents = new ObservableCollection<BusLineViewModel>();
            BusLineSuggestions = new ObservableCollection<BusLineViewModel>();

            SearchCommand = new NormalCommand(parameters =>
            {
                if (!string.IsNullOrWhiteSpace(BusLine))
                {
                    var url = UrlHelper.GetUrl("/BusLineDetailPage.xaml", new
                    {
                        SubLineName = BusLine,
                        Latitude = _parent.Latitude,
                        Longitude = _parent.Longitude
                    }, UriKind.RelativeOrAbsolute);
                    App.RootFrame.Navigate(url);
                }
            });

            ClickRecentCommand = new NormalCommand(parameters =>
            {
                if (parameters is BusLineViewModel)
                {
                    var subline = parameters as BusLineViewModel;
                    var url = UrlHelper.GetUrl("/BusLineDetailPage.xaml", new
                    {
                        SubLineID = subline.ID,
                        SubLineName = (BusLine = subline.Name),
                        Latitude = _parent.Latitude,
                        Longitude = _parent.Longitude
                    }, UriKind.RelativeOrAbsolute);
                    App.RootFrame.Navigate(url);
                }
            });

            RemoveRecentCommand = new NormalCommand(parameters =>
            {
                if (parameters is BusLineViewModel)
                {
                    var subline = parameters as BusLineViewModel;
                    _busManager.RemoveRecentSubline(subline.ID);
                    LoadRecent();
                }
            });
        }

        public MainPageViewModel_Bus(MainPageViewModel parent)
            : this()
        {
            _parent = parent;
        }

        public string BusLine { get; set; }

        public ObservableCollection<BusLineViewModel> Recents { get; private set; }

        public ICommand SearchCommand { get; private set; }

        public ICommand ClickRecentCommand { get; private set; }

        public ICommand RemoveRecentCommand { get; private set; }

        public bool IsHintEmptyRecentList
        {
            get { return base.GetValue<bool>("IsHintEmptyRecentList"); }
            set { base.SetValue("IsHintEmptyRecentList", value); }
        }

        public ObservableCollection<BusLineViewModel> BusLineSuggestions
        {
            get { return base.GetValue<ObservableCollection<BusLineViewModel>>("BusLineSuggestions"); }
            private set { base.SetValue("BusLineSuggestions", value); }
        }

        internal async void LoadRecent()
        {
            Recents.Clear();
            var recents = await _busManager.GetRecentSubList();
            if (recents != null)
                recents.ForEach(r => Recents.Add(EntityHelper.Transfer<BusLineViewModel>(r)));

            IsHintEmptyRecentList = Recents == null || Recents.Count == 0;
        }

        //private SuggestionsManager<BusLineViewModel, BusLineViewModel> _stationSuggestionManager = null;
        //internal void GetSuggestionsImpl(string key, Action callback)
        //{
        //    if (_stationSuggestionManager == null)
        //        _stationSuggestionManager = new SuggestionsManager<BusLineViewModel, BusLineViewModel>(
        //            this,
        //            _busManager.GetAllLines,
        //            BusLineSuggestions,
        //            item => item,
        //            item => item.Name);

        //    _stationSuggestionManager.GetSuggestions(key, callback);
        //}

        public void LoadAllLines()
        {
            if (BusLineSuggestions == null || BusLineSuggestions.Count == 0)
            {
                var items = _busManager.GetAllLines();
                BusLineSuggestions = new ObservableCollection<BusLineViewModel>(EntityHelper.Transfer<BusLineViewModel>(items));
            }
        }
    }

    class MainPageViewModel_Station : ViewModelBase
    {
        private MainPageViewModel _parent = null;
        private BusManager _busManager = BusManager.Instance;

        private MainPageViewModel_Station()
        {

        }

        public MainPageViewModel_Station(MainPageViewModel parent)
            : this()
        {
            _parent = parent;
            Stations = new ObservableCollection<BusStationViewModel>();
            StationSuggestions = new ObservableCollection<BusStationViewModel>();
            GetSuggestionsCommand = new NormalCommand(GetSuggestionsImpl);
            SubmitStationNameCommand = new NormalCommand(SubmitStationNameImpl);
            ClickStationCommand = new NormalCommand(GotoStaionDetail);
            IsHintEmptyStationList = false;
        }

        public string StationName
        {
            get { return base.GetValue<string>("StationName"); }
            set { base.SetValue("StationName", value); }
        }

        public ObservableCollection<BusStationViewModel> Stations { get; private set; }

        public ObservableCollection<BusStationViewModel> StationSuggestions { get; private set; }

        public ICommand GetSuggestionsCommand { get; private set; }

        private void GetSuggestionsImpl(object parameters)
        {
            if (parameters != null && !string.IsNullOrWhiteSpace(parameters.ToString()))
            {
                GetSuggestionsImpl(parameters.ToString(), null);
            }
        }

        private SuggestionsManager<BusStationViewModel, string> _stationSuggestionManager = null;
        public void GetSuggestionsImpl(string key, Action callback)
        {
            if (_stationSuggestionManager == null)
                _stationSuggestionManager = new SuggestionsManager<BusStationViewModel, string>(
                    this,
                    _busManager.GetStations,
                    StationSuggestions,
                    item => new BusStationViewModel() { name = item },
                    item => item);

            _stationSuggestionManager.GetSuggestions(key, callback);
        }

        public ICommand SubmitStationNameCommand { get; private set; }

        private void SubmitStationNameImpl(object stationName)
        {
            if (stationName != null && !string.IsNullOrWhiteSpace(stationName.ToString()))
            {
                var station = stationName.ToString();
                var funcSearchAndGotoStation = new Action<string>(name =>
                {
                    if (StationSuggestions.Count > 0)
                    {
                        if (StationSuggestions.FirstOrDefault(s => string.Equals(s.name, name, StringComparison.InvariantCultureIgnoreCase)) != null)
                        {
                            GotoStaionDetail(name);
                        }
                        else
                        {
                            StationName = StationSuggestions.First().name;
                            GotoStaionDetail(StationName);
                        }
                    }
                });

                if (StationSuggestions.Count == 0)
                {
                    GetSuggestionsImpl(station, () =>
                    {
                        funcSearchAndGotoStation(station);
                    });
                }
                else
                {
                    funcSearchAndGotoStation(station);
                }
            }
        }

        public ICommand ClickStationCommand { get; private set; }

        private void GotoStaionDetail(object stationName)
        {
            if (stationName != null && !string.IsNullOrWhiteSpace(stationName.ToString()))
            {
                var url = UrlHelper.GetUrl("/StationDetailPage.xaml", new
                {
                    StationName = stationName.ToString(),
                    Latitude = _parent.Latitude,
                    Longitude = _parent.Longitude
                }, UriKind.RelativeOrAbsolute);
                App.RootFrame.Navigate(url);
            }
        }

        public bool IsHintEmptyStationList
        {
            get { return base.GetValue<bool>("IsHintEmptyStationList"); }
            set { base.SetValue("IsHintEmptyStationList", value); }
        }

        private bool _hasLoadedNearStations = false;
        public async void LoadNearStations()
        {
            if (!_hasLoadedNearStations)
            {
                _parent.IsLoading = true;
                var ret = await _busManager.GetStations(_parent.Latitude, _parent.Longitude);
                if (ret != null)
                {
                    Stations.Clear();
                    if (ret.data != null)
                    {
                        ret.data.ForEach(i =>
                        {
                            if (Stations.FirstOrDefault(s => string.Equals(s.name, i.name, StringComparison.InvariantCultureIgnoreCase)) == null)
                            {
                                Stations.Add(new BusStationViewModel
                                {
                                    name = i.name
                                });
                            }
                        });
                    }
                    _hasLoadedNearStations = true;

                    IsHintEmptyStationList = Stations.Count == 0;
                }
                _parent.IsLoading = false;
            }
        }
    }

    class SuggestionsManager<VM, DTO>
    {
        ViewModelBase _viewModel = null;
        Func<string, Task<DTO[]>> _serverTask;
        ObservableCollection<VM> _list;
        Func<DTO, VM> _convertTo;
        Func<DTO, string> _getValue;

        public SuggestionsManager(
            ViewModelBase viewModel,
            Func<string, Task<DTO[]>> serverTask,
            ObservableCollection<VM> list,
            Func<DTO, VM> convertTo,
            Func<DTO, string> getValue)
        {
            _viewModel = viewModel;
            _serverTask = serverTask;
            _list = list;
            _convertTo = convertTo;
            _getValue = getValue;
        }

        private bool _isGettinhSuggestions = false;
        private Dictionary<string, DTO[]> _suggestionsCache = new Dictionary<string, DTO[]>();
        public async void GetSuggestions(string key, Action callback)
        {
            if (_isGettinhSuggestions)
            {
                if (callback != null) callback();
                return;
            }

            if (!string.IsNullOrWhiteSpace(key))
            {
                DTO[] items = null;

                var cacheKey = _suggestionsCache.Keys.FirstOrDefault(k => key.StartsWith(k));
                if (!string.IsNullOrWhiteSpace(cacheKey))
                {
                    items = _suggestionsCache[cacheKey].Where(v => (_getValue != null ? _getValue(v) : v.ToString()).Contains(key)).ToArray();
                }
                else
                {
                    _isGettinhSuggestions = true;
                    _viewModel.IsLoading = true;
                    Debug.WriteLine("获取:{0}", key);

                    items = await _serverTask(key);
                    if (items != null && !_suggestionsCache.ContainsKey(key))
                    {
                        _suggestionsCache.Add(key, items);
                    }
                    _viewModel.IsLoading = false;
                    _isGettinhSuggestions = false;
                }

                if (items != null)
                {
                    _list.Clear();
                    for (int i = 0; i < items.Length; i++)
                    {
                        var item = items[i];
                        _list.Add(_convertTo(item));
                    }
                }

                Debug.WriteLine("结束获取:{0}", key);
                if (callback != null) callback();
            }
        }
    }

    class MainPageViewModel_Settings : ViewModelBase
    {
        private MainPageViewModel _parent = null;

        public MainPageViewModel_Settings(MainPageViewModel parent)
            : this()
        {
            _parent = parent;
        }

        private MainPageViewModel_Settings()
        {
            IsEnableGeo = AppSettings.Current.IsEnableGeo;
            CanAutoRefresh = AppSettings.Current.CanAutoRefresh;
            NeedArrivedAlarm = true;//AppSettings.Current.NeedArrivedAlarm;
            NeedWaitingAlarm = AppSettings.Current.NeedWaitingAlarm;
            AutoRefreshIntervalItems = new List<DropdownItemViewModel<int>> { 
                new DropdownItemViewModel<int>("10秒",10),
                new DropdownItemViewModel<int>("20秒",20),
                new DropdownItemViewModel<int>("30秒",30),
                new DropdownItemViewModel<int>("60秒",60),
                new DropdownItemViewModel<int>("2分钟",120),
                new DropdownItemViewModel<int>("3分钟",180),
                new DropdownItemViewModel<int>("5分钟",300)
            };
            AutoRefreshInterval = AutoRefreshIntervalItems.FirstOrDefault(i => i.Value == AppSettings.Current.AutoRefreshInterval);
            if (AutoRefreshInterval == null)
                AutoRefreshInterval = AutoRefreshIntervalItems[2];

            WaitingAlarmThresholdItems = new List<DropdownItemViewModel<int>> { 
                new DropdownItemViewModel<int>("不提醒",0),
                new DropdownItemViewModel<int>("2站内",2),
                new DropdownItemViewModel<int>("3站内",3),
                new DropdownItemViewModel<int>("小于1分钟",60),
                new DropdownItemViewModel<int>("小于2分钟",120),
                new DropdownItemViewModel<int>("小于3分钟",180),
                new DropdownItemViewModel<int>("小于5分钟",300),
            };
            if (NeedWaitingAlarm == false)
            {
                WaitingAlarmThreshold = WaitingAlarmThresholdItems[0];
            }
            else
            {
                WaitingAlarmThreshold = WaitingAlarmThresholdItems.FirstOrDefault(i => i.Value == AppSettings.Current.WaitingAlarmThreshold);
                if (WaitingAlarmThreshold == null)
                {
                    WaitingAlarmThreshold = WaitingAlarmThresholdItems[2];
                }
            }

            ArrivedAlarmThresholdItems = new List<DropdownItemViewModel<int>> { 
                new DropdownItemViewModel<int>("300米",300),
                new DropdownItemViewModel<int>("500米",500),
                new DropdownItemViewModel<int>("800米",800),
                new DropdownItemViewModel<int>("1000米",1000)
            };
            ArrivedAlarmThreshold = ArrivedAlarmThresholdItems.FirstOrDefault(i => i.Value == AppSettings.Current.ArrivedAlarmThreshold);
            if (ArrivedAlarmThreshold == null)
                ArrivedAlarmThreshold = ArrivedAlarmThresholdItems[1];

            CityItems = SupportCity.CurrentSupport.Select(p => new DropdownItemViewModel<int>(p.Key, p.Value)).ToList();
        }

        public List<DropdownItemViewModel<int>> AutoRefreshIntervalItems { get; private set; }

        public List<DropdownItemViewModel<int>> WaitingAlarmThresholdItems { get; private set; }

        public List<DropdownItemViewModel<int>> ArrivedAlarmThresholdItems { get; private set; }

        public List<DropdownItemViewModel<int>> CityItems { get; private set; }

        public bool IsEnableGeo
        {
            get { return base.GetValue<bool>("IsEnableGeo"); }
            set
            {
                base.SetValue("IsEnableGeo", value);
                if (AppSettings.Current.IsEnableGeo != value)
                {
                    AppSettings.Current.IsEnableGeo = value;
                    AppSettings.Current.Save();
                }
            }
        }

        public bool CanAutoRefresh
        {
            get { return base.GetValue<bool>("CanAutoRefresh"); }
            set
            {
                base.SetValue("CanAutoRefresh", value);
                if (AppSettings.Current.CanAutoRefresh != value)
                {
                    AppSettings.Current.CanAutoRefresh = value;
                    AppSettings.Current.Save();
                }
            }
        }

        public DropdownItemViewModel<int> AutoRefreshInterval
        {
            get { return base.GetValue<DropdownItemViewModel<int>>("AutoRefreshInterval"); }
            set
            {
                base.SetValue("AutoRefreshInterval", value);
                if (value != null)
                {
                    if (AppSettings.Current.AutoRefreshInterval != value.Value)
                    {
                        AppSettings.Current.AutoRefreshInterval = value.Value;
                        AppSettings.Current.Save();
                    }
                }
            }
        }

        public bool NeedWaitingAlarm
        {
            get { return base.GetValue<bool>("NeedWaitingAlarm"); }
            set
            {
                base.SetValue("NeedWaitingAlarm", value);
                if (AppSettings.Current.NeedWaitingAlarm != value)
                {
                    AppSettings.Current.NeedWaitingAlarm = value;
                    AppSettings.Current.Save();
                }
            }
        }

        public DropdownItemViewModel<int> WaitingAlarmThreshold
        {
            get { return base.GetValue<DropdownItemViewModel<int>>("WaitingAlarmThreshold"); }
            set
            {
                base.SetValue("WaitingAlarmThreshold", value);
                if (value != null)
                {
                    if (AppSettings.Current.WaitingAlarmThreshold != value.Value)
                    {
                        AppSettings.Current.WaitingAlarmThreshold = value.Value;
                        if (value.Value <= 0)
                            AppSettings.Current.NeedWaitingAlarm = false;
                        else
                            AppSettings.Current.NeedWaitingAlarm = true;
                        AppSettings.Current.Save();
                    }
                }
            }
        }

        public bool NeedArrivedAlarm
        {
            get { return base.GetValue<bool>("NeedArrivedAlarm"); }
            set
            {
                base.SetValue("NeedArrivedAlarm", value);
                if (AppSettings.Current.NeedArrivedAlarm != value)
                {
                    AppSettings.Current.NeedArrivedAlarm = value;
                    AppSettings.Current.Save();
                }
            }
        }

        public DropdownItemViewModel<int> ArrivedAlarmThreshold
        {
            get { return base.GetValue<DropdownItemViewModel<int>>("ArrivedAlarmThreshold"); }
            set
            {
                base.SetValue("ArrivedAlarmThreshold", value);
                if (value != null)
                {
                    if (AppSettings.Current.ArrivedAlarmThreshold != value.Value)
                    {
                        AppSettings.Current.ArrivedAlarmThreshold = value.Value;
                        AppSettings.Current.Save();
                    }
                }
            }
        }

    }

    class MainPageViewModel_Plan : ViewModelBase
    {
        private MainPageViewModel _parent = null;
        private bool _useCurrentLocation = false;

        public MainPageViewModel_Plan(MainPageViewModel parent)
        {
            _parent = parent;
            if (_parent.HasLocatedPosition)
                Origin = _parent.Position;

            StartCommand = new NormalCommand(StartPlan);

            _useCurrentLocation = true;

            Suggestion = new ObservableCollection<string>();

            GetSuggestionCommand = new NormalCommand(GetSuggestion);
        }

        public string Origin
        {
            get { return base.GetValue<string>("Origin"); }
            set
            {
                base.SetValue("Origin", value);
                if (string.IsNullOrWhiteSpace(value))
                {
                    _useCurrentLocation = true;
                }
                else
                {
                    _useCurrentLocation = false;
                }
            }
        }

        public string Destination
        {
            get { return base.GetValue<string>("Destination"); }
            set { base.SetValue("Destination", value); }
        }

        public ObservableCollection<string> Suggestion { get; private set; }

        public ICommand StartCommand { get; private set; }

        private void StartPlan(object parameter)
        {
            if (string.IsNullOrWhiteSpace(Origin) && _parent.Longitude == 0 && _parent.Latitude == 0)
            {
                Helper.DisplayPrompt("没有输入起点");
                return;
            }

            if (string.IsNullOrWhiteSpace(Destination))
            {
                Helper.DisplayPrompt("没有输入终点");
                return;
            }

            var url = UrlHelper.GetUrl("/PlanResult.xaml", new
            {
                UseCurrentLocation = _useCurrentLocation,
                Origin = string.IsNullOrWhiteSpace(Origin) ? _parent.Position : Origin,
                Destination = Destination,
                Latitude = _parent.Latitude,
                Longitude = _parent.Longitude
            }, UriKind.RelativeOrAbsolute);
            App.RootFrame.Navigate(url);
        }

        public ICommand GetSuggestionCommand { get; private set; }

        SuggestionsManager<string, PlaceSuggestionItem> _stationSuggestionManager = null;

        private void GetSuggestion(object parameter)
        {
            GetSuggestionImpl(parameter, null);
        }

        internal void GetSuggestionImpl(object parameter, Action callback)
        {
            if (parameter != null)
            {
                IsLoading = true;
                if (_stationSuggestionManager == null)
                    _stationSuggestionManager = new SuggestionsManager<string, PlaceSuggestionItem>(
                        this,
                        LocationManager.Instance.GetPlaceSuggestion,
                        Suggestion,
                        item => item.name,
                        item => item.name);

                _stationSuggestionManager.GetSuggestions(parameter.ToString(), callback);
                IsLoading = false;
            }
        }

    }
}
