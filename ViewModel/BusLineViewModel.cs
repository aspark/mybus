﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.ViewModel
{
    public class BusLineViewModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string StartStation { get; set; }

        public string EndStation { get; set; }

        public long LastModified { get; set; }

        public bool IsOpen { get; set; }

    }
}
