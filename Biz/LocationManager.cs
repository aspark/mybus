﻿using myBus.Common;
using myBus.Model.Baidu;
using myBus.Model.Common;
using myBus.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Biz
{
    class LocationManager
    {
        private IBaiduClient _baiduClient = null;
        private LocationManager()
        {
            _baiduClient = ServerFactory.CreateBaiduClient(AppStatus.Current.CurrentCity);
        }

        private static LocationManager _instance = new LocationManager();
        public static LocationManager Instance
        {
            get {
                return _instance;
            }
        }

        public async Task<BaiduLocationResultDTO> GetPosition(double lat, double lng)
        {
            var ret = await _baiduClient.GetPositionAsync(lat, lng);

            return ret;
        }

        public void SetCity(string city)
        {
            if (!string.IsNullOrWhiteSpace(city) && !string.Equals(AppStatus.Current.CurrentCity, city))
            {
                AppStatus.Current.CurrentCity = city;
                _baiduClient.Region = AppStatus.Current.CurrentCity;
                if (BusManager.Instance.SwitchCity(AppStatus.Current.CurrentCity))
                {
                    Helper.DisplayPrompt(string.Format("公交查询区域：{0}", AppStatus.Current.CurrentCity));
                }
                else
                {
                    Helper.DisplayPrompt(string.Format("该地区（{0}）暂不支持实时公交查询", AppStatus.Current.CurrentCity), TimeSpan.FromSeconds(5));
                }
            }
        }

        public async Task<LBSResultBase<dynamic>> GetPlan(string start, string end, double? startLat = null, double? startLng = null)
        {
            return await _baiduClient.GetDirection(start, end, startLat, startLng);
        }

        public async Task<PlaceSuggestionItem[]> GetPlaceSuggestion(string key)
        {
            return await _baiduClient.GetPlaceSuggestion(key);
        }
    }
}
