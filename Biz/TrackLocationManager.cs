﻿using myBus.Common;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;

namespace myBus.Biz
{
    class TrackLocationManager : IDisposable
    {
        private double _targetLat, _targetLng;
        private int _threshold;
        public TrackLocationManager(double targetLat, double targetLng)
            : this(targetLat, targetLng, 0)
        {
        }
        public TrackLocationManager(double targetLat, double targetLng, int threshold)
        {
            var gpsPos = GPSConverter.MarToGPS(GPSConverter.BDToMar(new GeoCoordinate(targetLat, targetLng)));
            _targetLat = gpsPos.Latitude;
            _targetLng = gpsPos.Longitude;
            _threshold = threshold;
        }

        private Geolocator _geo = null;
        public void Start()
        {
            if (_geo == null)
            {
                _geo = new Geolocator();
                _geo.DesiredAccuracy = PositionAccuracy.High;
                _geo.MovementThreshold = 30;
                _geo.StatusChanged += _geo_StatusChanged;
                Microsoft.Phone.Shell.PhoneApplicationService.Current.ApplicationIdleDetectionMode = Microsoft.Phone.Shell.IdleDetectionMode.Disabled;
            }
        }

        async void _geo_StatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {
            switch (args.Status)
            {
                case PositionStatus.Disabled:
                    break;
                case PositionStatus.Initializing:
                    break;
                case PositionStatus.NoData:
                    break;
                case PositionStatus.NotAvailable:
                    break;
                case PositionStatus.NotInitialized:
                    break;
                case PositionStatus.Ready:
                    _geo.StatusChanged -= _geo_StatusChanged;
                    _geo.PositionChanged += _geo_PositionChanged;
                    OnTrackingStarted(EventArgs.Empty);
                    var position = await _geo.GetGeopositionAsync();
                    if (position != null)
                    {
                        ChangePosition(position.Coordinate.Latitude, position.Coordinate.Longitude);
                    }
                    break;
                default:
                    break;
            }
        }

        void _geo_PositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            ChangePosition(args.Position.Coordinate.Latitude, args.Position.Coordinate.Longitude);
        }

        private void ChangePosition(double lat, double lng)
        {
            double distance = CalcDistance(lat, lng);
            Debug.WriteLine("距离改变：{0}", distance);
            OnDistanceChanged(new DistanceChangedEventArgs(distance));
            if (_threshold > 0 && distance < _threshold)
            {
                OnArrived(EventArgs.Empty);
            }
        }

        public event EventHandler<EventArgs> Arrived;
        protected void OnArrived(EventArgs e)
        {
            if (Arrived != null)
            {
                Arrived(this, e);
            }
        }

        public event EventHandler<EventArgs> TrackingStarted;
        protected void OnTrackingStarted(EventArgs e)
        {
            if (TrackingStarted != null)
            {
                TrackingStarted(this, e);
            }
        }

        public event EventHandler<DistanceChangedEventArgs> DistanceChanged;
        protected void OnDistanceChanged(DistanceChangedEventArgs e)
        {
            if (DistanceChanged != null)
            {
                DistanceChanged(this, e);
            }
        }

        internal void Stop()
        {
            if (_geo != null)
            {
                _geo.StatusChanged -= _geo_StatusChanged;
                _geo.PositionChanged -= _geo_PositionChanged;
                _geo = null;
                //Microsoft.Phone.Shell.PhoneApplicationService.Current.ApplicationIdleDetectionMode = Microsoft.Phone.Shell.IdleDetectionMode.Enabled;
            }
        }

        private const double _earth_radius = 6378137;
        private static double rad(double d)
        {
            return d * Math.PI / 180.0;
        }
        
        //public double CalcDistanceGPS(double lat, double lng)
        //{
        //    var bdPos = GPSConverter.GPSToMar(new GeoCoordinate(lat, lng));

        //    return CalcDistance(bdPos.Latitude, bdPos.Longitude);
        //}

        public double CalcDistance(double lat, double lng)
        {
            double radLat1 = rad(lat);
            double radLat2 = rad(_targetLat);
            double a = radLat1 - radLat2;
            double b = rad(lng) - rad(_targetLng);

            double s = 2 * Math.Asin(Math.Sqrt(Math.Pow(Math.Sin(a / 2), 2) +
             Math.Cos(radLat1) * Math.Cos(radLat2) * Math.Pow(Math.Sin(b / 2), 2)));
            s = s * _earth_radius;
            s = Math.Round(s * 10000) / 10000;
            return s;
        }

        public void Dispose()
        {
            Stop();
        }
    }

    public class DistanceChangedEventArgs : EventArgs
    {
        public DistanceChangedEventArgs(double meters)
        {
            Meters = meters;
        }

        public double Meters { get; private set; }
    }
}
