﻿using myBus.Model;
using myBus.Model.KuMiKe;
using myBus.Model.Common;
using myBus.Repositories;
using myBus.Server;
using myBus.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using myBus.Common;
using System.Windows;
using System.Windows.Resources;
using System.IO.Compression;
using System.IO;
using Newtonsoft.Json;

namespace myBus.Biz
{
    class BusManager
    {
        private IBusClient _busClient = null;

        private BusManager()
        {
            _busClient = ServerFactory.CreateKuMiKeClient(AppStatus.Current.CurrentCity);
        }

        private static BusManager _instance = new BusManager();
        public static BusManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public async Task<List<RecentSubLine>> GetRecentSubList(int limit = 10)
        {
            List<RecentSubLine> subline = null;
            using (var context = new BusDataContext())
            {
                subline = await Task.Factory.StartNew<List<RecentSubLine>>(()=>{
                    return context.RecentSubLines.OrderByDescending(r => r.LastModified).Take(limit).ToList();
                });
            }

            return subline;
        }

        public async Task<NearBusExtraInfoDTO> GetNearBus(int sublineID, double latitude, double longitude)
        {
            //if local db is not exist the station use other api
            BusResultBase<NearBusExtraInfoDTO> ret = null;
            LineInfoDTO line = null;
            using (var context = new BusDataContext())
            {
                line = GetLineInfoFromLocal(sublineID);
            }

            ret = await _busClient.GetNearestBusListAsync(sublineID, latitude, longitude, line == null ? 0 : line.tm);
            UpdateStations(line, ret);
            if (ret.success)
                SaveRecentSubline(sublineID);

            if (ret != null)
            {
                return ret.data;
            }

            return null;
        }

        public async Task<NearBusExtraInfoDTO> GetNearBus(string busName, double latitude, double longitude)
        {
            //if local db is not exist the station use other api
            BusResultBase<NearBusExtraInfoDTO> ret = null;
            LineInfoDTO line = null;
            using (var context = new BusDataContext())
            {
                line = GetLineInfoFromLocal(busName);
            }

            if (line != null)
            {
                ret = await _busClient.GetNearestBusListAsync(line.SubLines[0].id, latitude, longitude, line.tm);
            }
            else
            {
                ret = await _busClient.GetNearestBusListAsync(busName, latitude, longitude);
            }

            UpdateStations(line, ret);
            if (ret.success && ret.data != null && ret.data.line>0)
                SaveRecentSubline(ret.data.line);

            if (ret != null)
            {
                return ret.data;
            }

            return null;
        }

        public async Task<NearBusInfoListDTO> GetNearBus(int lineID, int stationID, List<int> busIDList)
        {
            var ret = await _busClient.GetNearestBusListAsync(lineID, stationID, busIDList);
            return ret.data;
        }

        private LineInfoDTO GetLineInfoFromLocal(string busName)
        {
            using (var context = new BusDataContext())
            {
                LineInfoDTO line = context.LineInfos.ToList().FirstOrDefault(l => string.Equals(busName, l.name, StringComparison.InvariantCultureIgnoreCase));
                AppendSubLines(line);
                return line;
            }
        }

        private LineInfoDTO GetLineInfoFromLocal(int sublineID)
        {
            LineInfoDTO line = null;
            using (var context = new BusDataContext())
            {
                var subline = context.SubLines.FirstOrDefault(s => s.id == sublineID);
                if (subline != null)
                {
                    line = context.LineInfos.FirstOrDefault(l => l.id == subline.LineID);
                    AppendSubLines(line);
                }
            }
            return line;
        }

        private void AppendSubLines(LineInfoDTO line)
        {
            if (line != null)
            {
                using (var context = new BusDataContext())
                {
                    line.SubLines = context.SubLines.Where(s => s.LineID == line.id).ToList();
                    line.SubLines.ForEach(d =>
                    {
                        d.stations = context.Stations.Where(s => s.LineID == d.id).ToList();
                    });
                }
            }
        }

        private void UpdateStations(LineInfoDTO line, BusResultBase<NearBusExtraInfoDTO> ret)
        {
            if (ret != null)
            {
                if (ret.data == null || ret.data.dir == null)
                {
                    if (line != null)
                    {
                        if (ret.data == null)
                            ret.data = new NearBusExtraInfoDTO();
                        ret.data.lineinfo = line;
                        ret.data.dir = line.SubLines;
                    }
                }
                else
                {
                    ret.data.lineinfo.SubLines = new List<SubLineInfoDTO>();
                    ret.data.lineinfo.SubLines.AddRange(ret.data.dir);
                    line = ret.data.lineinfo;
                    line.tm = ret.tm;

                    UpdateStations(line);
                }
            }

        }

        private void SaveRecentSubline(int subLineID)
        {
            using (var context = new BusDataContext())
            {
                var existRecent = context.RecentSubLines.FirstOrDefault(r => r.ID == subLineID);
                if (existRecent != null)
                {
                    existRecent.LastModified = DateTime.Now.Ticks;
                }
                else
                {
                    var subline = context.SubLines.FirstOrDefault(s => s.id == subLineID);
                    if (subline != null)
                    {
                        var line = context.LineInfos.FirstOrDefault(l => l.id == subline.LineID);
                        if (line != null)
                        {
                            existRecent = new RecentSubLine();
                            existRecent.ID = subLineID;
                            existRecent.LastModified = DateTime.Now.Ticks;
                            existRecent.Name = line.name;
                            existRecent.StartStation = subline.start_station;
                            existRecent.EndStation = subline.end_station;
                            context.RecentSubLines.InsertOnSubmit(existRecent);
                        }
                    }
                }

                context.SubmitChanges();
            }
        }

        private void UpdateStations(LineInfoDTO line)
        {
            if (line != null)
            {
                if (line.SubLines != null)
                {
                    foreach (var subline in line.SubLines)
                    {
                        subline.LineID = line.id;

                        if (subline.stations != null)
                        {
                            subline.stations.ForEach(s => s.LineID = subline.id);
                        }
                    }
                }

                using (var context = new BusDataContext())
                {
                    var oldLine = context.LineInfos.FirstOrDefault(l => l.id == line.id);
                    if (oldLine != null)
                    {
                        oldLine.tm = line.tm;

                        oldLine.SubLines = context.SubLines.Where(s => s.LineID == oldLine.id).ToList();
                        oldLine.SubLines.ForEach(subLine => {
                            context.Stations.DeleteAllOnSubmit(context.Stations.Where(s=>s.LineID==subLine.id));
                            context.SubLines.DeleteOnSubmit(subLine);
                        });
                    }
                    else
                    {
                        context.LineInfos.InsertOnSubmit(line);
                    }

                    if (line.SubLines != null)
                    {
                        context.SubLines.InsertAllOnSubmit(line.SubLines);
                        context.Stations.InsertAllOnSubmit(line.SubLines.SelectMany(s => s.stations));
                    }

                    context.Refresh(System.Data.Linq.RefreshMode.KeepChanges);
                    context.SubmitChanges();
                }
            }
        }

        private RecentSubLine[] _allLines = new RecentSubLine[0];
        public RecentSubLine[] GetAllLines()
        {
            if (_allLines == null || _allLines.Length == 0)
            {
                List<AllLinesItemDTO> lines = null;
                long lastRequestTime = 0;
                using (var context = new BusDataContext())
                {
                    var linesInfo = context.RequestInfos.FirstOrDefault(r => r.Name == AllLinesItemDTO.RequestInfoName);
                    if (linesInfo != null)
                    {
                        lastRequestTime = linesInfo.TimeStamp;
                    }

                    lines = context.AllLines.ToList();
                    if (lines == null || lines.Count == 0)
                    {
                        lastRequestTime = 0;
                    }
                }

                UpdateLocalLines(lastRequestTime);

                if (lines != null)
                {
                    _allLines = lines.Select(l => new RecentSubLine
                    {
                        ID = l.id,
                        Name = l.name,
                        StartStation = l.start_station,
                        EndStation = l.end_station,
                        IsOpen = l.isopen == EnumIsOPEN.TRUE
                    }).ToArray();
                }
            }

            return _allLines;
        }

        private void UpdateLocalLines(long lastRequestTime)
        {
            _busClient.GetAllLines(lastRequestTime).ContinueWith(s =>
            {
                var ret = s.Result;
                if (ret.data != null && ret.data.Count > 0)
                {
                    //lines = ret.data;
                    using (var context = new BusDataContext())
                    {
                        context.AllLines.DeleteAllOnSubmit(context.AllLines);
                        context.AllLines.InsertAllOnSubmit(ret.data);
                        var requestInfo = context.RequestInfos.FirstOrDefault(r => r.Name == AllLinesItemDTO.RequestInfoName);
                        if (requestInfo != null)
                        {
                            requestInfo.TimeStamp = ret.tm;
                        }
                        else
                        {
                            requestInfo = new RequestInfo()
                            {
                                Name = AllLinesItemDTO.RequestInfoName,
                                TimeStamp = ret.tm
                            };
                            context.RequestInfos.InsertOnSubmit(requestInfo);
                        }
                        context.SubmitChanges();
                    }
                }
            });
        }

        public void RemoveRecentSubline(int subLineID)
        {
            if (subLineID > 0)
            {
                using (var context = new BusDataContext())
                {
                    var existRecent = context.RecentSubLines.FirstOrDefault(r => r.ID == subLineID);
                    if (existRecent != null)
                    {
                        context.RecentSubLines.DeleteOnSubmit(existRecent);
                        context.SubmitChanges();
                    }
                }
            }
        }

        public bool SwitchCity(string city)
        {
            if (SupportCity.CurrentSupport.ContainsKey(city))
            {
                _busClient.CityCode = SupportCity.CurrentSupport[city].ToString();
                return true;
            }

            return false;
        }

        public Task<string[]> GetStations(string key)
        {
            return _busClient.GetStations(key);
        }

        public Task<BusResultBase<List<StationDTO>>> GetStations(double lat, double lng)
        {
            return _busClient.GetStations(lat, lng);
        }

        internal async Task<BusResultBase<List<StationLineItem>>> GetStationDetail(string stationName)
        {
            return await _busClient.GetStationDetail(stationName);
        }
    }
}
