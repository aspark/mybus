﻿using myBus.Internals;
using myBus.Model.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace myBus.Biz
{
    class PriceManager
    {
        private PriceManager()
        {

        }

        private static PriceManager _instance = new PriceManager();
        public static PriceManager Instance
        {
            get
            {
                return _instance;
            }
        }
        
        private static List<Tuple<string, Table>> _busLinePriceCollection = null;
        private static object _busLinePriceCollectionLock = new object();
        public Task<Dictionary<int, float>> GetPrice(string busLine, string currentStationName, string endStationName, int colIndex = -1)
        {
            return Task.Factory.StartNew(()=>{

                #region Load Data List
                if (_busLinePriceCollection == null)
                {
                    lock (_busLinePriceCollectionLock)
                    {
                        if (_busLinePriceCollection == null)
                        {
                            _busLinePriceCollection = new List<Tuple<string, Table>>();
                            var fileInfo = Application.GetResourceStream(new Uri("Repositories/Data/Price.zip", UriKind.RelativeOrAbsolute));
                            if (fileInfo != null && fileInfo.Stream != null)
                            {
                                if (fileInfo != null && fileInfo.Stream != null)
                                {
                                    using (var archive = new Unzip(fileInfo.Stream))
                                    {
                                        foreach (var entry in archive.Entries)
                                        {
                                            using (var ms = new MemoryStream())
                                            {
                                                archive.Extract(entry, ms);
                                                ms.Position = 0;
                                                using (var sr = new StreamReader(ms))
                                                {
                                                    var entity = JsonConvert.DeserializeObject<Table>(sr.ReadToEnd());
                                                    _busLinePriceCollection.Add(new Tuple<string, Table>(entry.Name, entity));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } 
                #endregion

                Dictionary<int, float> dicPrice = new Dictionary<int, float>();
                foreach (var item in _busLinePriceCollection.Where(p => p.Item2.Name.Contains(busLine)).OrderBy(p => p.Item2.Name).ToList()) 
                {
                    var cols = item.Item2.R[0].C.Select(c => c.N).ToArray();
                    var directFlag = (CalcSimlarStation(endStationName, new string[] { cols.First(), cols.Last() }) > 0) ? 1 : -1;
                    if (colIndex < 0 || colIndex > cols.Length || colIndex < 0)
                    {
                        colIndex = CalcSimlarStation(currentStationName, cols);
                    }
                    else if (directFlag < 0 && cols.Length > colIndex && colIndex > -1)
                    {
                        colIndex = (cols.Length - 1 - colIndex);
                    }

                    if (item.Item2.R.Count > colIndex)
                    {
                        var row = item.Item2.R[colIndex];
                        float price = 2;
                        int stationCount = 0;
                        dicPrice.Add(stationCount++, price);
                        for (var i = colIndex + directFlag; i > -1 && i < row.C.Count; i += directFlag)
                        {
                            if (row.C[i].P > price)
                            {
                                price = row.C[i].P;
                                dicPrice.Add(stationCount, price);
                            }
                            stationCount++;
                        }

                        break;
                    }
                }

                return dicPrice;
            });
        }

        private int CalcSimlarStation(string name, string[] stations)
        {
            int[] arrCount = new int[stations.Length];
            for (int i = 0; i < name.Length;i++ )
            {
                for (int j = 0; j < stations.Length; j++)
                {
                    if (stations[j].Contains(name[i]))
                    {
                        arrCount[j]++;
                    }
                }
            }

            return arrCount.ToList().IndexOf(arrCount.Max());
        }
    }
}
