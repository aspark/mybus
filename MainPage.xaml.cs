﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using myBus.Resources;
using myBus.ViewModel;
using System.Device.Location;
using Windows.Devices.Geolocation;
using System.Diagnostics;
using System.Windows.Input;
using myBus.Common;

namespace myBus
{
    public partial class MainPage : PhoneApplicationPage, IDisposable
    {
        private MainPageViewModel _viewModel = null;

        public MainPage()
        {
            InitializeComponent();

            txtBusLine.KeyUp += txtBusLine_KeyUp;
            txtBusLine.Populating += txtBusLine_Populating;
            //((ApplicationBarMenuItem)ApplicationBar.MenuItems[0]).Click += btnAbout_Click;
            plMain.SelectionChanged += plMain_SelectionChanged;

            txtStationName.KeyUp += txtStationName_KeyUp;
            txtStationName.Populating += txtStationName_Populating;

            btnSendEmail.Click += delegate {
                Helper.LaunchEmail();
            };

            btnSendComment.Click += delegate {
                Microsoft.Phone.Tasks.MarketplaceReviewTask task = new Microsoft.Phone.Tasks.MarketplaceReviewTask();
                task.Show();
            };

            txtPlanSuggestion.Populating += txtPlanSuggestion_Populating;
            txtPlanSuggestion.KeyUp += txtPlanSuggestion_KeyUp;
            txtPlanSuggestion.GotFocus += txtPlanSuggestion_GotFocus;
            txtPlanStart.Click += txtPlanStart_Click;
            txtPlanEnd.Click += txtPlanEnd_Click;
        }

        void txtPlanSuggestion_GotFocus(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("got focus");
            txtPlanSuggestion.LostFocus -= txtPlanSuggestion_LostFocus;
            txtPlanSuggestion.LostFocus += txtPlanSuggestion_LostFocus;
        }

        void txtPlanSuggestion_LostFocus(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("lost focus");
            txtPlanSuggestion.LostFocus -= txtPlanSuggestion_LostFocus;
            ClosePlanSuggestionPop();
        }

        void txtPlanSuggestion_Populating(object sender, PopulatingEventArgs e)
        {
            e.Cancel = true;
            _viewModel.PlanViewModel.GetSuggestionImpl(e.Parameter, txtPlanSuggestion.PopulateComplete);
        }

        void txtPlanSuggestion_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ClosePlanSuggestionPop();
            }
        }

        void txtPlanEnd_Click(object sender, RoutedEventArgs e)
        {
            ShowPlanSuggestionPop(lblPlanEnd.Text, txtPlanEnd);
        }

        void txtPlanStart_Click(object sender, RoutedEventArgs e)
        {
            ShowPlanSuggestionPop(lblPlanStart.Text, txtPlanStart);
        }

        private void ShowPlanSuggestionPop(string label, FrameworkElement input)
        {
            lblPlanSuggestion.Text = string.Format("{0}：", label);
            var textProperty = txtPlanSuggestion.GetBindingExpression(AutoCompleteBox.TextProperty);
            txtPlanSuggestion.SetBinding(AutoCompleteBox.TextProperty, new System.Windows.Data.Binding {
                Path = new PropertyPath("Content"),
                ElementName = input.Name,
                Mode = System.Windows.Data.BindingMode.TwoWay
            });
            popPlanInput.IsOpen = true;
            txtPlanSuggestion.Focus();
        }

        private void ClosePlanSuggestionPop()
        {
            popPlanInput.IsOpen = false;
            //txtPlanSuggestion.SetBinding(AutoCompleteBox.TextProperty, null);
            txtPlanSuggestion.ClearValue(AutoCompleteBox.TextProperty);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (_viewModel == null)
            {
                _viewModel = new MainPageViewModel();
#if DEBUG
                _viewModel.BusViewModel.BusLine = "311";
#endif
                this.DataContext = _viewModel;
                _viewModel.BusViewModel.LoadRecent();
            }
            else
            {
                if (plMain.SelectedItem == null || plMain.SelectedItem == iBusLine)
                {
                    _viewModel.BusViewModel.LoadRecent();
                }
            }

            base.OnNavigatedTo(e);
        }

        private bool _waitingPressBackAgain = false;
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (!_waitingPressBackAgain)
            {
                e.Cancel = true;
                _waitingPressBackAgain = true;
                var duration = TimeSpan.FromSeconds(2);
                Helper.DisplayPrompt(plRoot, "再按一次退出", duration);
                Windows.System.Threading.ThreadPoolTimer.CreateTimer(t =>
                {
                    _waitingPressBackAgain = false;
                }, duration);
            }

            base.OnBackKeyPress(e);
        }

        protected override void OnRemovedFromJournal(JournalEntryRemovedEventArgs e)
        {
            Dispose();
            base.OnRemovedFromJournal(e);
        }

        void txtBusLine_Populating(object sender, PopulatingEventArgs e)
        {
            _viewModel.BusViewModel.LoadAllLines();
            //e.Cancel = true;
            //_viewModel.BusViewModel.GetSuggestionsImpl(e.Parameter, txtStationName.PopulateComplete);
        }

        void txtStationName_Populating(object sender, PopulatingEventArgs e)
        {
            e.Cancel = true;
            _viewModel.StationViewModel.GetSuggestionsImpl(e.Parameter, txtStationName.PopulateComplete);
        }

        void txtStationName_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                this.Focus();
                txtStationName.GetBindingExpression(AutoCompleteBox.TextProperty).UpdateSource();
                _viewModel.StationViewModel.SubmitStationNameCommand.Execute(txtStationName.Text);
            }
        }

        void plMain_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (plMain.SelectedItem == iStation)
            {
                _viewModel.StationViewModel.LoadNearStations();
            }
        }

        void btnAbout_Click(object sender, EventArgs e)
        {
            plMain.SelectedItem = piAbout;
        }

        void txtBusLine_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                this.Focus();
                txtBusLine.GetBindingExpression(AutoCompleteBox.TextProperty).UpdateSource();
                _viewModel.BusViewModel.SearchCommand.Execute(txtBusLine.Text);
            }
        }

        public void Dispose()
        {
            if(_viewModel!=null)
            {
                _viewModel.Dispose();
            }
        }
    }
}