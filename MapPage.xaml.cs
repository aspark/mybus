﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Maps.Toolkit;
using myBus.Common;
using System.Device.Location;

namespace myBus
{
    public partial class MapPage : PhoneApplicationPage
    {
        public MapPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            List<GeoCoordinate> points = new List<GeoCoordinate>();
            var pnt = GPSConverter.GPSToMar(new GeoCoordinate(AppStatus.Current.CurrentLat, AppStatus.Current.CurrentLng));
            mpMain.Center = pnt;
            mpMain.ZoomLevel = 16;
            UserLocationMarker marker = new UserLocationMarker();
            MapExtensions.Add(MapExtensions.GetChildren(mpMain), marker, pnt);
            points.Add(pnt);

            if (CurrentRoute.Steps != null && CurrentRoute.Steps.Count > 0)
            {
                Pushpin pin = null;
                for (int i = 0; i < CurrentRoute.Steps.Count;i++ )
                {
                    var step = CurrentRoute.Steps[i];
                    var local = step.DefaultVehicleViewModel.EndLocation;
                    pnt = GPSConverter.BDToMar(new GeoCoordinate(local.lat, local.lng));
                    pin = new Pushpin();
                    pin.Content = string.Format("{0}:{1}", i + 1, step.DefaultVehicleViewModel.EndName.Default(i == 0 ? "起点" : "终点"));
                    MapExtensions.Add(MapExtensions.GetChildren(mpMain), pin, pnt);
                    points.Add(pnt);
                }

                var polyline = new Microsoft.Phone.Maps.Controls.MapPolyline();
                //polyline.Path = new Microsoft.Phone.Maps.Controls.GeoCoordinateCollection();
                points.ForEach(p => polyline.Path.Add(p));
                mpMain.MapElements.Add(polyline);
            }

            base.OnNavigatedTo(e);
        }

        internal static ViewModel.PlanResultRouteViewModel CurrentRoute { get; set; }
    }
}