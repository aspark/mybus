﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using myBus.ViewModel;
using System.Windows.Automation.Peers;
using myBus.Common;

namespace myBus
{
    public partial class BusDetailPage : PhoneApplicationPage
    {
        public BusDetailPage()
        {
            InitializeComponent();

            var btnRefresh__ = ApplicationBar.Buttons[0] as ApplicationBarIconButton;
            btnRefresh__.Click += btnRefresh_Click;
            var btnSwitchDirection__ = ApplicationBar.Buttons[1] as ApplicationBarIconButton;
            btnSwitchDirection__.Click += btnSwitchDirection_Click;

        }

        private BusLineDetailPageViewModel _viewModel = null;

        void btnSwitchDirection_Click(object sender, EventArgs e)
        {
            if (_viewModel != null)
            {
                _viewModel.SwitchDirectionCommand.Execute(null);
            }
        }

        void btnRefresh_Click(object sender, EventArgs e)
        {
            if (_viewModel != null)
            {
                _viewModel.RefreshCommand.Execute(null);
            }
        }

        NavigationMode _lastNavigatedToMode;
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _lastNavigatedToMode = e.NavigationMode;
            if (e.NavigationMode == NavigationMode.Reset)
                App.CurrentApp.RemoveCheckForResetNavigation();

            if (_viewModel == null)
            {
                _viewModel = new BusLineDetailPageViewModel();
                _viewModel.CurrentStationChanged += _viewModel_FilledStations;

                this.FillPropertyFromQuery(_viewModel);
                this.DataContext = _viewModel;
                _viewModel.RefreshCommand.Execute(null);

                if (AppSettings.Current.NeedWaitingAlarm && AppSettings.Current.CanAutoRefresh)
                {
                    PhoneApplicationService.Current.ApplicationIdleDetectionMode = IdleDetectionMode.Disabled;
                    PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
                    Helper.DisplayPrompt(plRoot, "候车提醒时，请不要离开此页面");
                }
            }
            else
            {
                if (_viewModel.TrackingViewModel.CanStopTracking)
                {
                    _viewModel.TrackingViewModel.StopTrackLocation();
                }
            }

            base.OnNavigatedTo(e);
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            if (_lastNavigatedToMode == NavigationMode.Reset)
            {
                e.Cancel = true;
                _lastNavigatedToMode = NavigationMode.New;
                return;
            }
            if (e.IsNavigationInitiator)
            {
                _viewModel.TrackingViewModel.StopTrackLocation();
                try
                {
                    PhoneApplicationService.Current.ApplicationIdleDetectionMode = IdleDetectionMode.Enabled;
                }
                catch { }

                try
                {
                    PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Enabled;
                }
                catch { }
            }

            base.OnNavigatingFrom(e);
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (_viewModel.TrackingViewModel.IsTrackingLocation)
            {
                Helper.DisplayPrompt(plRoot, "退出到站提醒模式");
                _viewModel.TrackingViewModel.StopTrackLocation();
                e.Cancel = true;
            }

            base.OnBackKeyPress(e);
        }

        void _viewModel_FilledStations(object sender, EventArgs e)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                var currentStation = _viewModel.Stations.FirstOrDefault(s => s.IsCurrent);
                UIElement itemContainer = plStations.ItemContainerGenerator.ContainerFromItem(currentStation) as UIElement;
                if (itemContainer != null)
                {
                    //ItemAutomationPeer.
                    System.Windows.Media.VisualTreeHelper.GetParent(itemContainer);
                    var peer = FrameworkElementAutomationPeer.CreatePeerForElement(itemContainer);
                    if (peer != null)
                    {
                        var offset = FrameworkElementAutomationPeer.CreatePeerForElement(itemContainer).GetBoundingRectangle().Left/(App.Current.Host.Content.ScaleFactor/100.0);
                        if (offset < 30 || offset > 370)
                        {
                            offset = offset - (double.IsNaN(plStationsMain.ActualWidth) ? 0 : plStationsMain.ActualWidth) / 2;
                            plStationsMain.ScrollToHorizontalOffset(offset + plStationsMain.HorizontalOffset);
                        }
                    }
                }
            });
        }
    }
}