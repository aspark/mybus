﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Model.Common
{
    [Table]
    public class RecentSubLine
    {
        [Column(IsPrimaryKey = true)]
        public int ID { get; set; }

        [Column]
        public string Name { get; set; }

        [Column]
        public string StartStation { get; set; }

        [Column]
        public string EndStation { get; set; }

        [Column]
        public long LastModified { get; set; }

        public bool IsOpen { get; set; }
    }
}
