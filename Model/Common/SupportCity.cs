﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Model.Common
{
    public class SupportCity
    {
        public readonly static Dictionary<string, int> CurrentSupport = new Dictionary<string, int> { 
            {"北京市",860201},
            {"深圳市",860515},
            {"西安市",862307}
        };
    }
}
