﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Model.Common
{
    public class Table
    {
        public Table()
        {
            R = new List<Row>();
        }

        public string Name { get; set; }

        public List<Row> R { get; set; }
    }

    public class Row
    {
        public Row()
        {
            C = new List<Col>();
        }

        public string N { get; set; }

        public List<Col> C { get; set; }
    }

    public class Col
    {
        public string N { get; set; }

        public float P { get; set; }
    }
}
