﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Model.Common
{
    [Table]
    public class RequestInfo
    {
        //public RequestInfo()
        //{
        //    ID = Guid.NewGuid();
        //}

        //[Column]
        //public Guid ID { get; set; }

        [Column(IsPrimaryKey = true)]
        public string Name { get; set; }

        [Column]
        public long TimeStamp { get; set; }
    }
}
