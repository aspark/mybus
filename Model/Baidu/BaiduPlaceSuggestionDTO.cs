﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Model.Baidu
{
    public class BaiduPlaceSuggestionDTO : BaiduResultBase<PlaceSuggestionItem[]>
    {


    }

    public class PlaceSuggestionItem
    {
        public string name { get; set; }
        //public string city{ get; set; }
        //public string district{ get; set; }
        //public string business{ get; set; }
        public string cityid { get; set; }
    }
}
