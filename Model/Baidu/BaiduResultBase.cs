﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Model.Baidu
{
    public class BaiduResultBase<T>
    {
        public int status { get; set; }
        public T result { get; set; }
    }

    public class LBSResultBase<T> : BaiduResultBase<T>
    {
        public string message { get; set; }
        public int type { get; set; }
    }

    //public class Copyright
    //{
    //    public string text { get; set; }
    //    public string imageUrl { get; set; }
    //}

    //public class Info
    //{
    //    public Copyright copyright { get; set; }
    //}

    public class LocationDTO
    {
        public double lng { get; set; }
        public double lat { get; set; }
    }

}
