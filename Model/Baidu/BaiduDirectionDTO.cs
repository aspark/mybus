﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Model.Baidu
{
    public class SchemeDTO
    {
        public int distance { get; set; }
        public int duration { get; set; }
        public List<StepChoice> steps { get; set; }
        public LocationDTO originLocation { get; set; }
        public LocationDTO destinationLocation { get; set; }
    }

    public class StepChoice : List<StepDTO>
    { }

    public class StepDTO
    {
        public int distance { get; set; }
        public int duration { get; set; }
        /// <summary>
        /// 道路类型
        /// </summary>
        public string type { get; set; }
        public string path { get; set; }
        public string tip { get; set; }
        public string tip_text { get; set; }
        public string sname { get; set; }
        public LocationDTO stepOriginLocation { get; set; }
        public LocationDTO stepDestinationLocation { get; set; }
        public string stepInstruction { get; set; }

        public VehicleDTO vehicle { get; set; }
    }

    public class VehicleDTO
    {
        public string end_name { get; set; }
        public string end_time { get; set; }
        public string end_uid { get; set; }
        public string name { get; set; }
        public string start_name { get; set; }
        public string start_time { get; set; }
        public string start_uid { get; set; }
        public int stop_num { get; set; }
        public int total_price { get; set; }
        public int type { get; set; }
        public string uid { get; set; }
        public int zone_price { get; set; }
    }

    public class RouteDTO
    {
        public List<SchemeDTO> scheme { get; set; }
    }

    public class OriginLocationDTO
    {
        public LocationDTO originPt { get; set; }
    }

    public class DestinationLocationDTO
    {
        public LocationDTO destinationPt { get; set; }
    }

    public class TaxiDetailDTO
    {
        public string desc { get; set; }
        public string km_price { get; set; }
        public string start_price { get; set; }
        public string total_price { get; set; }
    }

    public class TaxiDTO
    {
        public List<TaxiDetailDTO> detail { get; set; }
        public int distance { get; set; }
        public int duration { get; set; }
        public string remark { get; set; }
    }

    public class DirectionResultDTO
    {
        public List<RouteDTO> routes { get; set; }
        public OriginLocationDTO origin { get; set; }
        public DestinationLocationDTO destination { get; set; }
        public TaxiDTO taxi { get; set; }
    }

    //public class DirectionDTO : LBSResultBase<DirectionResultDTO>
    //{
    //    //public string message { get; set; }
    //    //public int type { get; set; }
    //    //public Info info { get; set; }
    //}

    public class PlaceDTO
    {
        public string name { get; set; }
        public string address { get; set; }
        public string uid { get; set; }
        public LocationDTO location { get; set; }
    }

    public class CityDTO
    {
        public string cname { get; set; }
        public int code { get; set; }
    }

    public class SearchInfoDTO
    {
        /// <summary>
        /// search key
        /// </summary>
        public string wd { get; set; }
        public CityDTO city { get; set; }
    }

    public class DirectionHintResultDTO
    {
        public List<PlaceDTO> destination { get; set; }
        public SearchInfoDTO destinationInfo { get; set; }
        public SearchInfoDTO originInfo { get; set; }
        public List<PlaceDTO> origin { get; set; }
    }

    //public class DirectionHintDTO : LBSResultBase<DirectionHintResultDTO>
    //{
    //    //public string message { get; set; }
    //    //public int type { get; set; }
    //    //public Info info { get; set; }
    //}
}
