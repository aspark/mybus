﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Model.Baidu
{
    public class BaiduGeoDTO : BaiduResultBase<BaiduLocationResultDTO>
    {
        //public int status { get; set; }
        //public BaiduLocationResultDTO result { get; set; }
    }

    public class BaiduLocationResultDTO
    {
        public LocationDTO location { get; set; }
        public string formatted_address { get; set; }
        public string business { get; set; }
        public AddressComponent addressComponent { get; set; }
        public int cityCode { get; set; }
    }

    public class AddressComponent
    {
        public string city { get; set; }
        public string district { get; set; }
        public string province { get; set; }
        public string street { get; set; }
        public string street_number { get; set; }
    }
}
