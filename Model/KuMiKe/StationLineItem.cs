﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Model.KuMiKe
{
    public class StationLineItem
    {
        public string begin_time { get; set; }
        public string dir { get; set; }
        public string end_station { get; set; }
        public string end_station2 { get; set; }
        public string end_time { get; set; }
        public EnumIsOPEN isopen { get; set; }
        public string line_name { get; set; }
        public string price { get; set; }
        public string start_station { get; set; }
        public CurrentStationDTO station { get; set; }
        public int sublineid { get; set; }
        public int? sublineid2 { get; set; }
        public long wait_time { get; set; }

        public class CurrentStationDTO
        {
            public int id { get; set; }
            public double lat { get; set; }
            public double lng { get; set; }
            public string name { get; set; }
        }
    }
}
