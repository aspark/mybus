﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Model.KuMiKe
{
    public class NearBusInfoListDTO : List<BusInfoDTO>
    {
        
    }

    public class NearBusExtraInfoDTO
    {
        public List<BusInfoDTO> cars { get; set; }
        public List<SubLineInfoDTO> dir { get; set; }
        public int line { get; set; }
        public LineInfoDTO lineinfo { get; set; }
        public StationDTO station { get; set; }
    }
}
