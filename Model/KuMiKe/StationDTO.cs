﻿using Microsoft.Phone.Data.Linq.Mapping;
using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Model.KuMiKe
{
    [Table]
    [Index(Columns = "id", IsUnique = true)]
    public class StationDTO
    {
        [Column]
        public int distance { get; set; }

        [Column(IsPrimaryKey = true)]
        public int id { get; set; }

        [Column]
        public double lat { get; set; }

        [Column]
        public double lng { get; set; }

        [Column]
        public string name { get; set; }

        [Column]
        public int seq { get; set; }

        [Column]
        public int waittime { get; set; }

        [Column]
        public int LineID { get; set; }

        public string code { get; set; }//addded in near stations
    }
}
