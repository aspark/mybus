﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Model.KuMiKe
{
    public class BusResultBase<T>
    {
        public bool success { get; set; }

        public string msg { get; set; }

        public string errcode { get; set; }

        public long tm { get; set; }

        public T data { get; set; }
    }
}
