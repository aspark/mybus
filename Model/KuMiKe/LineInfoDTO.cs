﻿using Microsoft.Phone.Data.Linq.Mapping;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Model.KuMiKe
{
    [Table]
    public class LineInfoDTO
    {
        [Column(IsPrimaryKey = true)]
        public int id { get; set; }

        [Column]
        public string name { get; set; }

        [Column]
        public long tm { get; set; }

        //[Association(ThisKey = "id", OtherKey = "LineID")]
        public List<SubLineInfoDTO> SubLines { get; set; }

        [Column]
        public EnumIsOPEN isopen { get; set; }

        [Column]
        public string original_name { get; set; }
    }

    public enum EnumIsOPEN : short
    {
        FALSE = 0,
        TRUE = 1
    }
}
