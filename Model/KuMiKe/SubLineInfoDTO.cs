﻿using Microsoft.Phone.Data.Linq.Mapping;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Model.KuMiKe
{
    [Table]
    [Index(Columns = "id", IsUnique = true)]
    public class SubLineInfoDTO
    {
        [Column]
        public short direction { get; set; }

        [Column]
        public string end_station { get; set; }

        [Column(IsPrimaryKey = true)]
        public int id { get; set; }

        [Column]
        public string price { get; set; }

        [Column]
        public string service_time { get; set; }//service_time=05:00-22:00

        [Column]
        public string start_station { get; set; }

        //[Association(ThisKey="id", OtherKey = "LineID")]
        public List<StationDTO> stations { get; set; }

        [Column]
        public int LineID { get; set; }
    }

}
