﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Model.KuMiKe
{
    public class BusInfoDTO
    {
        public int carid { get; set; }
        public long distance { get; set; }
        public double lat { get; set; }
        public double lng { get; set; }
        public string name { get; set; }
        public int stationid { get; set; }
        public EnumStationState stationstate { get; set; }
        public long waittime { get; set; }
    }

    public enum EnumStationState : short
    {
        Arriving = 1,
        Leave = 2,
        Comming = 3
    }
}
