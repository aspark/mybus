﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Model.KuMiKe
{
    [Table]
    public class AllLinesItemDTO
    {
        public static readonly string RequestInfoName = "AllLinesItemDTO";

        [Column]
        public short direction { get; set; }

        [Column]
        public string end_station { get; set; }

        [Column(IsPrimaryKey = true)]
        public int id { get; set; }

        [Column]
        public EnumIsOPEN isopen { get; set; }

        [Column]
        public string name { get; set; }

        [Column]
        public string original_name { get; set; }

        [Column]
        public string service_time { get; set; }

        [Column]
        public string start_station { get; set; }

        [Column]
        public Guid ParentID { get; set; }

    }
}
