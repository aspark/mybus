﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using myBus.ViewModel;

namespace myBus
{
    public partial class StationDetailPage : PhoneApplicationPage
    {
        StationDetailPageViewModel _viewModel = null;
        public StationDetailPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (_viewModel == null)
            {
                _viewModel = new StationDetailPageViewModel();
                this.DataContext = _viewModel;
                this.FillPropertyFromQuery(_viewModel);
                _viewModel.GetLines();
            }

            base.OnNavigatedTo(e);
        }
    }
}