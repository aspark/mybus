﻿using Microsoft.Phone.Data.Linq;
using Microsoft.Phone.Data.Linq.Mapping;
using myBus.Model.Common;
using myBus.Model.KuMiKe;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Resources;

namespace myBus.Repositories.Migrations
{
    class Migrations_V3
    {
        private const int version = 3;
        private static readonly string _oldConnStr = "DataSource=isostore:/bus.sdf";

        public void Execute()
        {
            using (var oldContext = new BusDataContext_V2(_oldConnStr))
            {
                if (oldContext.DatabaseExists())
                {
                    oldContext.ObjectTrackingEnabled = false;
                    using (var newContext = new BusDataContext())
                    {
                        try
                        {
                            //newContext is v4
                            newContext.LineInfos.InsertAllOnSubmit(oldContext.LineInfos);
                            newContext.SubLines.InsertAllOnSubmit(oldContext.SubLines.ToList().Select(o => new SubLineInfoDTO()
                            {
                                direction = o.direction,
                                end_station = o.end_station,
                                id = o.id,
                                price = o.price.ToString(),
                                service_time = o.service_time,
                                start_station = o.start_station,
                                LineID = o.LineID
                            }));
                            newContext.Stations.InsertAllOnSubmit(oldContext.Stations);
                            newContext.RecentSubLines.InsertAllOnSubmit(oldContext.RecentSubLines);
                            ////newContext.RequestInfos.InsertAllOnSubmit(oldContext.RequestInfos);
                            ////newContext.AllLines.InsertAllOnSubmit(oldContext.AllLines);

                            ////BusDataContext.IntialAllLines();
                            ////fileInfo.Stream
                            newContext.SubmitChanges();
                        }
                        catch { }
                    }

                    oldContext.DeleteDatabase();
                }
            }
        }
    }

    class BusDataContext_V2 : BusDataContext
    {
        public BusDataContext_V2(string conn)
            : base(conn)
        {

        }

        public new Table<SubLineInfoDTO_V2> SubLines = null;

    }

    [Table(Name = "SubLineInfoDTO")]
    [Index(Columns = "id", IsUnique = true)]
    public class SubLineInfoDTO_V2
    {
        [Column]
        public short direction { get; set; }

        [Column]
        public string end_station { get; set; }

        [Column(IsPrimaryKey = true)]
        public int id { get; set; }

        [Column]
        public decimal? price { get; set; }

        [Column]
        public string service_time { get; set; }//service_time=05:00-22:00

        [Column]
        public string start_station { get; set; }

        //[Association(ThisKey="id", OtherKey = "LineID")]
        public List<StationDTO> stations { get; set; }

        [Column]
        public int LineID { get; set; }
    }
}
