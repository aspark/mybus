﻿using myBus.Model;
using myBus.Model.KuMiKe;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Phone.Data.Linq;
using myBus.Model.Common;
using myBus.Repositories.Migrations;
using System.Windows;
using System.Windows.Resources;
using Newtonsoft.Json;
using System.IO;

namespace myBus.Repositories
{
    class BusDataContext : DataContext
    {
        public const int CurrentVersion = 4;
        private static readonly string _connStr = "DataSource=isostore:/bus_v4.sdf";
        public BusDataContext()
            : base(_connStr)
        {
        }

        public BusDataContext(string fileOrConnection)
            : base(fileOrConnection)
        {
        }

        public static void InitialDB()
        {
            using (var context = new BusDataContext())
            {
                //#if DEBUG
                //                //context.DeleteDatabase();
                //#endif

                if (!context.DatabaseExists())
                {
                    context.CreateDatabase();
                    var updater = context.CreateDatabaseSchemaUpdater();
                    updater.DatabaseSchemaVersion = BusDataContext.CurrentVersion;
                    updater.Execute();

                    IntialAllLines();

                    new Migrations_V3().Execute();
                    new Migrations_V4().Execute();
                }
                //else
                //{
                //    var updater = context.CreateDatabaseSchemaUpdater();
                //    //new Migrations_V1().Update(updater);
                //    //new Migrations_V2().Update(updater);
                //}
            }
        }

        public static void IntialAllLines()
        {
            using (var newContext = new BusDataContext())
            {
                if (newContext.AllLines.Count() == 0 && newContext.RequestInfos.FirstOrDefault(r => r.Name == AllLinesItemDTO.RequestInfoName) == null)
                {
                    var fileInfo = Application.GetResourceStream(new Uri("Repositories/Data/Json.zip", UriKind.RelativeOrAbsolute));
                    if (fileInfo != null && fileInfo.Stream != null)
                    {
                        StreamResourceInfo zipInfo = new StreamResourceInfo(fileInfo.Stream, fileInfo.ContentType);
                        fileInfo = Application.GetResourceStream(zipInfo, new Uri("JsonAllLines.txt", UriKind.RelativeOrAbsolute));
                        if (fileInfo != null && fileInfo.Stream != null)
                        {
                            using (var sr = new StreamReader(fileInfo.Stream))
                            {
                                var entity = JsonConvert.DeserializeObject<BusResultBase<List<AllLinesItemDTO>>>(sr.ReadToEnd());
                                newContext.AllLines.InsertAllOnSubmit(entity.data);
                                newContext.RequestInfos.InsertOnSubmit(new RequestInfo()
                                {
                                    Name = AllLinesItemDTO.RequestInfoName,
                                    TimeStamp = entity.tm
                                });
                            }
                        }
                    }
                }
            }
        }

        public Table<LineInfoDTO> LineInfos = null;

        public Table<SubLineInfoDTO> SubLines = null;

        public Table<StationDTO> Stations = null;

        public Table<RecentSubLine> RecentSubLines = null;

        public Table<RequestInfo> RequestInfos = null;

        public Table<AllLinesItemDTO> AllLines = null;
    }
}
