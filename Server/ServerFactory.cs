﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Server
{
    class ServerFactory
    {
        public static IBaiduClient CreateBaiduClient(string region)
        {
#if FAKE
            return new BaiduClient_Fake();
#else
            return new BaiduClient(region);
#endif
        }

        public static IBusClient CreateKuMiKeClient(string cityCode)
        {

#if FAKE 
            return new BusClient_Fake();
#else
            return new BusClient(cityCode);
#endif
        }
    }
}
