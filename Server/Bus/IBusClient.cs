﻿using myBus.Model.KuMiKe;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace myBus.Server
{
    interface IBusClient
    {
        string CityCode { get; set; }
        Task<BusResultBase<List<AllLinesItemDTO>>> GetAllLines(long lastLoadedTime = 0);
        Task<BusResultBase<NearBusExtraInfoDTO>> GetNearestBusListAsync(int lineID, double lat, double lng, long lastLoadedTime = 0);
        Task<BusResultBase<NearBusInfoListDTO>> GetNearestBusListAsync(int lineID, int stationId, global::System.Collections.Generic.List<int> busIDList);
        Task<BusResultBase<NearBusExtraInfoDTO>> GetNearestBusListAsync(string busCode, double lat, double lng, long lastLoadedTime = 0);
        Task<string[]> GetStations(string key);
        Task<BusResultBase<List<StationDTO>>> GetStations(double lat, double lng);
        Task<BusResultBase<List<StationLineItem>>> GetStationDetail(string stationName);
    }
}
