﻿using myBus.Model;
using myBus.Model.KuMiKe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Server
{
    class BusClient : IBusClient
    {
        internal RequestHelper _requestHelper = null;
        internal BusClient()
        {
            _requestHelper = new RequestHelper();
            CityCode = "860515";
        }

        internal BusClient(string cityCode):this()
        {
            if(!string.IsNullOrWhiteSpace(cityCode))
                CityCode = cityCode;
        }

        public string CityCode { get; set; }

        private async Task<BusResultBase<T>> GetAsync<T>(string url)
        {
            BusResultBase<T> result = null;
            var retWeb = await _requestHelper.GetAsync<BusResultBase<dynamic>>(url);
            if (retWeb != null)
            {
                result = new BusResultBase<T>();
                result.success = retWeb.success;
                result.msg = retWeb.msg;
                result.tm = retWeb.tm;
                if (retWeb.success && retWeb.data != null)
                {
                    result.data = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(retWeb.data.ToString().Replace("NULL", ""));
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="busLine"></param>
        /// <param name="lat">纬度</param>
        /// <param name="lng">经度</param>
        /// <param name="lastLoadedTime">最后获取线路的时间，为0时返回线路所有的站点</param>
        public async Task<BusResultBase<NearBusExtraInfoDTO>> GetNearestBusListAsync(int lineID, double lat, double lng, long lastLoadedTime = 0)
        {
            string url = GetNearestBusListUrl(lineID, lat, lng, lastLoadedTime);
            return await GetAsync<NearBusExtraInfoDTO>(url);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="busLine"></param>
        /// <param name="lat">纬度</param>
        /// <param name="lng">经度</param>
        public async Task<BusResultBase<NearBusExtraInfoDTO>> GetNearestBusListAsync(string busCode, double lat, double lng, long lastLoadedTime = 0)
        {
            string url = GetNearestBusListUrl(busCode, lat, lng, lastLoadedTime);
            return await GetAsync<NearBusExtraInfoDTO>(url);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lineID">线路号</param>
        /// <param name="stationId">站台号</param>
        /// <param name="busIDList">公车号</param>
        /// <param name="callback"></param>
        public async Task<BusResultBase<NearBusInfoListDTO>> GetNearestBusListAsync(int lineID, int stationId, List<int> busIDList)
        {
            string url = GetNearestBusListUrl(lineID, stationId, busIDList);
            return await GetAsync<NearBusInfoListDTO>(url);
        }

        public async Task<string[]> GetStations(string key)
        {
            string url = GetStationsUrl(key);
            var ret = await _requestHelper.GetAsync<BusResultBase<List<dynamic>>>(url);
            if (ret != null && ret.success && ret.data != null)
            {
                return ret.data.Select(d => (string)d.station_name.ToString()).ToArray();
            }

            return new string[0];
        }

        public async Task<BusResultBase<List<StationDTO>>> GetStations(double lat, double lng)
        {
            string url = GetStationsUrl(lat, lng);
            return await GetAsync<List<StationDTO>>(url);
        }

        public async Task<BusResultBase<List<StationLineItem>>> GetStationDetail(string stationName)
        {
            string url = GetStationDetailUrl(stationName);
            return await GetAsync<List<StationLineItem>>(url);
        }

        public async Task<BusResultBase<List<AllLinesItemDTO>>> GetAllLines(long lastLoadedTime = 0)
        {
            string url = GetAllLinesUrl(lastLoadedTime);
            return await GetAsync<List<AllLinesItemDTO>>(url);
        }

        #region Urls

        private readonly string _host = "http://busapi.gpsoo.net:80".TrimEnd('/');

        private string GetNearestBusListUrl(int lineID, double lat, double lng, long lastLoadedTime = 0)
        {
            //http://busapi.gpsoo.net:80/v1/bus/mbcommonservice?method=getnearcarinfov4&sublineid=99058&mapType=BAIDU&citycode=860515&cn=gm&posmaptype=BAIDU&lastmodi=0&lat=22.535061&lng=114.075842
            var query = new Dictionary<string, object>();
            query.Add("method", "getnearcarinfov4");
            query.Add("sublineid", lineID);
            query.Add("mapType", "BAIDU");
            query.Add("citycode", CityCode);
            query.Add("cn", "gm");
            query.Add("posmaptype", "BAIDU");
            query.Add("lat", lat);
            query.Add("lng", lng);
            query.Add("lastmodi", lastLoadedTime);//DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds
            return UrlHelper.GetUrl(_host + "/v1/bus/mbcommonservice", query);
        }

        private string GetNearestBusListUrl(string name, double lat, double lng, long lastLoadedTime = 0)
        {
            //http://busapi.gpsoo.net:80/v1/bus/mbcommonservice?method=getnearcarinfov3&line_name=311&mapType=BAIDU_MAP&citycode=860515&lng=114.075947&lat=22.544599&posmaptype=BAIDU_MAP&cn=gm&lastmodi=1396920751
            var query = new Dictionary<string, object>();
            query.Add("method", "getnearcarinfov3");
            query.Add("line_name", name);
            query.Add("mapType", "BAIDU_MAP");
            query.Add("citycode", CityCode);
            query.Add("lng", lng);
            query.Add("lat", lat);
            query.Add("posmaptype", "BAIDU_MAP");
            query.Add("cn", "gm");
            query.Add("lastmodi", lastLoadedTime);//DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds
            return UrlHelper.GetUrl(_host + "/v1/bus/mbcommonservice", query);
        }

        private string GetNearestBusListUrl(int lineID, int stationId, List<int> busIDList)
        {
            //http://busapi.gpsoo.net:80/v1/bus/mbcommonservice?method=getnearcarinfo&cn=gm&ids=&sublineid=99438&stationId=1916046&mapType=BAIDU&citycode=860515
            var query = new Dictionary<string, object>();
            query.Add("method", "getnearcarinfo");
            query.Add("cn", "gm");
            query.Add("ids", busIDList == null ? "" : string.Join(",", busIDList));
            query.Add("sublineid", lineID);
            query.Add("stationId", stationId);
            query.Add("mapType", "BAIDU");
            query.Add("citycode", CityCode);
            return UrlHelper.GetUrl(_host + "/v1/bus/mbcommonservice", query);
        }

        private string GetStationsUrl(string key)
        {
            //GET /v1/bus/mbcommonservice?method=getmatchedstations&citycode=860515&mapType=G_NORMAL_MAP&cn=gm&stationname=%E8%8E%B2%E5%A1%98%E7%AB%99 HTTP/1.1
            var query = new Dictionary<string, object>();
            query.Add("method", "getmatchedstations");
            query.Add("citycode", CityCode);
            query.Add("mapType", "G_NORMAL_MAP");
            query.Add("cn", "gm");
            query.Add("stationname", key);
            return UrlHelper.GetUrl(_host + "/v1/bus/mbcommonservice", query);
        }

        private string GetStationsUrl(double lat, double lng)
        {
            //GET /v1/bus/mbcommonservice?method=get_station_refpos&lng=114.076059&lat=22.544495&mapType=BAIDU_MAP&posmaptype=BAIDU_MAP&citycode=860515 HTTP/1.1
            var query = new Dictionary<string, object>();
            query.Add("method", "get_station_refpos");
            query.Add("lng", lng);
            query.Add("lat", lat);
            query.Add("mapType", "BAIDU_MAP");
            query.Add("posmaptype", "BAIDU_MAP");
            query.Add("citycode", CityCode);
            return UrlHelper.GetUrl(_host + "/v1/bus/mbcommonservice", query);
        }

        private string GetStationDetailUrl(string stationName)
        {
            ///v1/bus/mbcommonservice?method=get_line_by_station&station_name=%E6%B7%B1%E5%A4%A7%E5%8C%97%E9%97%A8%E2%91%A0&citycode=860515 HTTP/1.1
            var query = new Dictionary<string, object>();
            query.Add("method", "get_line_by_station");
            query.Add("station_name", stationName.Replace("(", "").Replace(")", ""));
            query.Add("citycode", CityCode);
            return UrlHelper.GetUrl(_host + "/v1/bus/mbcommonservice", query);
        }

        private string GetAllLinesUrl(long lastLoadedTime = 0)
        {
            //GET /v1/bus/mbcommonservice?method=get_all_lines&citycode=860515&mapType=BAIDU&cn=gm&lastmodi=0 HTTP/1.1
            var query = new Dictionary<string, object>();
            query.Add("method", "get_all_lines");
            query.Add("citycode", CityCode);
            query.Add("mapType", "BAIDU");
            query.Add("cn", "gm");
            query.Add("lastmodi", lastLoadedTime);
            return UrlHelper.GetUrl(_host + "/v1/bus/mbcommonservice", query);
        }

        #endregion

    }
}
