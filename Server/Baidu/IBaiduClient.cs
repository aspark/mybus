﻿using System;
using myBus.Model.Baidu;
using System.Threading.Tasks;
namespace myBus.Server
{
    interface IBaiduClient
    {
        string Region { get; set; }
        Task<BaiduLocationResultDTO> GetPositionAsync(double lat, double lng);
        Task<LBSResultBase<dynamic>> GetDirection(string originName, string destination, double? originLat = null, double? originLng = null);
        Task<PlaceSuggestionItem[]> GetPlaceSuggestion(string key);
    }
}
