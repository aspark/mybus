﻿using myBus.Model.Baidu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Server
{
    class BaiduClient : IBaiduClient
    {
        private RequestHelper _requestHelper = null;
        internal BaiduClient()
        {
            _requestHelper = new RequestHelper();
            Region = "深圳";
        }

        internal BaiduClient(string region)
            : this()
        {
            if(!string.IsNullOrWhiteSpace(region))
                Region = region;
        }

        public string Region { get; set; }

        //private async Task<BaiduResultBase<T>> GetAsync<T>(string url)
        //{
        //    BaiduResultBase<T> result = null;
        //    var retWeb = await _requestHelper.GetAsync<BaiduResultBase<dynamic>>(url);
        //    if (retWeb != null)
        //    {
        //        result = new BaiduResultBase<T>();
        //        result.status = retWeb.status;
        //        if (retWeb.status == 0 && retWeb.result != null)
        //        {
        //            result.result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(retWeb.result.ToString().Replace("NULL", ""));
        //        }
        //    }

        //    return result;
        //}

        public async Task<BaiduLocationResultDTO> GetPositionAsync(double lat, double lng)
        {
            string url = GetPositionUrl(lat, lng);
            var ret = await _requestHelper.GetAsync<BaiduGeoDTO>(url);
            if (ret.status == 0 && ret.result != null)
            {
                return ret.result;
            }

            return null;
        }

        //public void ConvertToBaiduLocation(double lat, double lng, Action<double, double> callback)
        //{ 
            
        //}

        public async Task<LBSResultBase<dynamic>> GetDirection(string originName, string destination, double? originLat = null, double? originLng = null)
        {
            string url = GetDirectionUrl(originName, destination, originLat, originLng);
            var ret = await _requestHelper.GetAsync<LBSResultBase<dynamic>>(url);
            if (ret.type == 2)
            {
                ret.result = Newtonsoft.Json.JsonConvert.DeserializeObject<DirectionResultDTO>(ret.result.ToString().Replace("NULL", ""));
            }
            else if (ret.type == 1)
            {
                ret.result = Newtonsoft.Json.JsonConvert.DeserializeObject<DirectionHintResultDTO>(ret.result.ToString().Replace("NULL", ""));
            }

            return ret;
        }

        public async Task<PlaceSuggestionItem[]> GetPlaceSuggestion(string key)
        {
            string url = GetPlaceSuggestionUrl(key);
            var ret = await _requestHelper.GetAsync<BaiduPlaceSuggestionDTO>(url);
            if (ret.status == 0 && ret.result != null)
            {
                return ret.result;
            }

            return null;
        }

        #region Urls

        private readonly string _host = "http://api.map.baidu.com";
        private string _ak = "LQNhWgvY8XsClwpf4qW0Dvfg";

        private string GetPositionUrl(double lat, double lng)
        {
            //http://api.map.baidu.com/geocoder/v2/?ak=E4805d16520de693a3fe707cdc962045&callback=renderReverse&location=39.983424,116.322987&output=json&pois=1
            Dictionary<string, object> query = new Dictionary<string, object>();
            query.Add("ak", _ak);
            query.Add("output", "json");
            query.Add("coordtype", "wgs84ll");
            query.Add("location", string.Format("{0},{1}", lat, lng));

            return UrlHelper.GetUrl(_host + "/geocoder/v2/", query);
        }

        private string GetDirectionUrl(string originName, string destination, double? originLat = null, double? originLng = null)
        {
            //http://api.map.baidu.com/direction/v1?mode=transit&origin=%E6%8B%9B%E8%A1%8C&destination=%E7%81%AB%E8%BD%A6&region=%E6%B7%B1%E5%9C%B3&output=json&ak=LQNhWgvY8XsClwpf4qW0Dvfg
            Dictionary<string, object> query = new Dictionary<string, object>();
            originName = originName ?? "";
            if (originLat.HasValue && originLng.HasValue)
                query.Add("origin", string.Format("{0},{1}", originLat.Value, originLng.Value));
            else
                query.Add("origin", originName);
            query.Add("destination", destination);
            query.Add("mode", "transit");
            query.Add("region", Region);
            query.Add("output", "json");
            query.Add("coord_type", "bd09ll");
            query.Add("ak", _ak);

            return UrlHelper.GetUrl(_host + "/direction/v1", query);
        }

        private string GetPlaceSuggestionUrl(string key)
        {
            //http://api.map.baidu.com/place/v2/suggestion?query=%E5%B2%97%E5%8E%A6&region=%E6%B7%B1%E5%9C%B3%E5%B8%82&output=json&ak=LQNhWgvY8XsClwpf4qW0Dvfg
            Dictionary<string, object> query = new Dictionary<string, object>();
            query.Add("query", key);
            query.Add("region", Region);
            query.Add("output", "json");
            query.Add("ak", _ak);

            return UrlHelper.GetUrl(_host + "/place/v2/suggestion", query);
        }

        #endregion

    }
}
