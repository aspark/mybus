﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
//using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace myBus.Server
{
    sealed class RequestHelper
    {
        public RequestHelper()
        {

        }

        private Encoding ContentEncoding = Encoding.UTF8;

        public async Task<string> GetAsync(string url)
        {
            Debug.WriteLine("开始请求:{0}", url);

            using (var handler = new HttpClientHandler())
            {
                handler.AutomaticDecompression = DecompressionMethods.GZip;
                using (HttpClient request = new HttpClient(handler))
                {
                    request.Timeout = TimeSpan.FromMinutes(3);
                    string content = await request.GetStringAsync(url); 

                    //var response = await request.GetAsync(url);
                    //if (response.IsSuccessStatusCode)
                    //{
                    //    await response.Content.ReadAsStringAsync();
                    //}

                    Debug.WriteLine("结束请求:\r\n{0}\r\n{1}", url, content);
                    return content;
                }
            }

            //return "";

            //HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            //request.Method = "Get";
            //request.Headers["Accept-Encoding"] = "gzip";

            //using (var response = await request.GetResponseAsync())
            //{
            //    using (var s = response.GetResponseStream())
            //    {
            //        using (var dr = new GZipStream(s, CompressionMode.Decompress))
            //        {
            //            using (var sr = new StreamReader(dr, ContentEncoding, true))
            //            {
            //                var content = sr.ReadToEnd();
            //                Debug.WriteLine("结束请求:\r\n{0}\r\n{1}", url, content);
            //                return content;
            //            }
            //        }
            //    }
            //}

        }

        //public void Get(string url, Action<string> callback)
        //{
        //    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
        //    request.Method = "Get";
        //    request.Headers["Accept-Encoding"] = "gzip";

        //    Debug.WriteLine("开始请求:{0}", url);

        //    request.BeginGetResponse(ar =>
        //    {
        //        try
        //        {
        //            using (HttpWebResponse response = request.EndGetResponse(ar) as HttpWebResponse)
        //            {
        //                if (callback != null)
        //                {
        //                    using (var s = response.GetResponseStream())
        //                    {
        //                        using (var dr = new GZipStream(s, CompressionMode.Decompress))
        //                        {
        //                            using (var sr = new StreamReader(dr, ContentEncoding, true))
        //                            {
        //                                var content = sr.ReadToEnd();
        //                                Debug.WriteLine("结束请求:\r\n{0}\r\n{1}", url, content);
        //                                callback(content);
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Debug.WriteLine(ex);
        //        }
        //    }, null);
        //}

        public async Task<T> GetAsync<T>(string url)
        {
            var content = await GetAsync(url);
            if (!string.IsNullOrWhiteSpace(content))
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(content);
            }

            return default(T);
        }

        //public void Get<T>(string url, Action<T> callback)
        //{
        //    Get(url, str => {
        //        if (callback != null)
        //        {
        //            if (!string.IsNullOrWhiteSpace(str))
        //            {
        //                callback(Newtonsoft.Json.JsonConvert.DeserializeObject<T>(str));
        //                return;
        //            }

        //            callback(default(T));
        //        }
        //    });
        //}

        public async Task<string> PostAsync(string url, Dictionary<string, object> values)
        {

            using (var httpHandler = new HttpClientHandler())
            {
                httpHandler.AutomaticDecompression = DecompressionMethods.GZip;
                //httpHandler.
                using (var request = new HttpClient(httpHandler))
                {
                    request.Timeout = TimeSpan.FromMinutes(3);
                    var content = new StringContent(UrlHelper.GetUrlQuery(values));
                    var response = await request.PostAsync(url, content);
                    response.EnsureSuccessStatusCode();
                    return await response.Content.ReadAsStringAsync();
                }
            }



            //var request = (HttpWebRequest)HttpWebRequest.Create(url);
            //request.Method = "Post";
            //request.Headers["Accept-Encoding"] = "gzip";
            //var rs = await request.GetRequestStreamAsync();
            //var content = UrlHelper.GetUrlQuery(values);
            //request.ContentLength = content.Length;
            //using (var sw = new StreamWriter(rs))
            //{
            //    sw.Write(content);
            //}

            //using (HttpWebResponse response = ((HttpWebResponse)await request.GetResponseAsync()))
            //{
            //    using (var s = response.GetResponseStream())
            //    {
            //        using (var dr = new GZipStream(s, CompressionMode.Decompress))
            //        {
            //            using (var sr = new StreamReader(dr, ContentEncoding, true))
            //            {
            //                return sr.ReadToEnd();
            //            }
            //        }
            //    }
            //}
        }

        //public void Post(string url, Dictionary<string, object> values, Action<string> callback)
        //{
        //    var request = HttpWebRequest.Create(url);
        //    request.Method = "Post";
        //    request.Headers["Accept-Encoding"] = "gzip";
        //    request.BeginGetRequestStream(ar =>
        //    {
        //        using (var rs = request.EndGetRequestStream(ar))
        //        {
        //            if (values != null)
        //            {
        //                var content = UrlHelper.GetUrlQuery(values);
        //                request.ContentLength = content.Length;
        //                using (var sw = new StreamWriter(rs))
        //                {
        //                    sw.Write(content);
        //                }

        //                request.BeginGetResponse(arr =>
        //                {
        //                    using (HttpWebResponse response = request.EndGetResponse(arr) as HttpWebResponse)
        //                    {
        //                        if (callback != null)
        //                        {
        //                            using (var s = response.GetResponseStream())
        //                            {
        //                                using (var dr = new GZipStream(s, CompressionMode.Decompress))
        //                                {
        //                                    using (var sr = new StreamReader(dr, ContentEncoding, true))
        //                                    {
        //                                        callback(sr.ReadToEnd());
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                }, null);
        //            }
        //        }
        //    }, null);
        //}
    }

    static class RequestExtension
    {
        public static Task<HttpWebResponse> GetResponseAsync(this HttpWebRequest request)
        {
            return Task.Factory.FromAsync<HttpWebResponse>(request.BeginGetResponse, ar => (HttpWebResponse)request.EndGetResponse(ar), null).ContinueWith(r => {
                if (r.Exception != null)
                {
                    return null;
                }
                return r.Result; 
            });
        }

        public static Task<Stream> GetRequestStreamAsync(this HttpWebRequest request)
        {
            return Task.Factory.FromAsync<Stream>(request.BeginGetRequestStream, request.EndGetRequestStream, null);
        }
    }
}
