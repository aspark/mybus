﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Common
{
    class GPSConverter
    {
        #region GPSToMar
        const double pi = 3.14159265358979324;

        //
        // Krasovsky 1940
        //
        // a = 6378245.0, 1/f = 298.3
        // b = a * (1 - f)
        // ee = (a^2 - b^2) / a^2;
        const double a = 6378245.0;
        const double ee = 0.00669342162296594323;

        //
        // World Geodetic System ==> Mars Geodetic System
        public static GeoCoordinate GPSToMar(GeoCoordinate wgPos)
        {
            GeoCoordinate mgPos = new GeoCoordinate(wgPos.Latitude, wgPos.Longitude);
            if (!outOfChina(wgPos.Latitude, wgPos.Longitude))
            {
                double dLat = transformLat(wgPos.Longitude - 105.0, wgPos.Latitude - 35.0);
                double dLon = transformLon(wgPos.Longitude - 105.0, wgPos.Latitude - 35.0);
                double radLat = wgPos.Latitude / 180.0 * pi;
                double magic = Math.Sin(radLat);
                magic = 1 - ee * magic * magic;
                double sqrtMagic = Math.Sqrt(magic);
                dLat = (dLat * 180.0) / ((a * (1 - ee)) / (magic * sqrtMagic) * pi);
                dLon = (dLon * 180.0) / (a / sqrtMagic * Math.Cos(radLat) * pi);
                mgPos.Latitude = wgPos.Latitude + dLat;
                mgPos.Longitude = wgPos.Longitude + dLon;
            }

            return mgPos;
        }

        public static GeoCoordinate MarToGPS(GeoCoordinate mgPos)
        {
            GeoCoordinate wgPos = new GeoCoordinate(mgPos.Latitude, mgPos.Longitude);
            var tempPos1 = GPSToMar(mgPos);
            //var tempPos2 = MarToGPS(mgPos);
            wgPos.Latitude -= (tempPos1.Latitude - mgPos.Latitude);
            wgPos.Longitude -= (tempPos1.Longitude - mgPos.Longitude);

            return wgPos;
        }

        private static bool outOfChina(double lat, double lon)
        {
            if (lon < 72.004 || lon > 137.8347)
                return true;
            if (lat < 0.8293 || lat > 55.8271)
                return true;
            return false;
        }

        private static double transformLat(double x, double y)
        {
            double ret = -100.0 + 2.0 * x + 3.0 * y + 0.2 * y * y + 0.1 * x * y + 0.2 * Math.Sqrt(Math.Abs(x));
            ret += (20.0 * Math.Sin(6.0 * x * pi) + 20.0 * Math.Sin(2.0 * x * pi)) * 2.0 / 3.0;
            ret += (20.0 * Math.Sin(y * pi) + 40.0 * Math.Sin(y / 3.0 * pi)) * 2.0 / 3.0;
            ret += (160.0 * Math.Sin(y / 12.0 * pi) + 320 * Math.Sin(y * pi / 30.0)) * 2.0 / 3.0;
            return ret;
        }

        private static double transformLon(double x, double y)
        {
            double ret = 300.0 + x + 2.0 * y + 0.1 * x * x + 0.1 * x * y + 0.1 * Math.Sqrt(Math.Abs(x));
            ret += (20.0 * Math.Sin(6.0 * x * pi) + 20.0 * Math.Sin(2.0 * x * pi)) * 2.0 / 3.0;
            ret += (20.0 * Math.Sin(x * pi) + 40.0 * Math.Sin(x / 3.0 * pi)) * 2.0 / 3.0;
            ret += (150.0 * Math.Sin(x / 12.0 * pi) + 300.0 * Math.Sin(x / 30.0 * pi)) * 2.0 / 3.0;
            return ret;
        } 
        #endregion

        #region MarToBD
        private const double x_pi = 3.14159265358979324 * 3000.0 / 180.0;

        public static GeoCoordinate GPSToBD(GeoCoordinate wgPos)
        {
            return MarToBD(GPSToMar(wgPos));
        }

        public static GeoCoordinate MarToBD(GeoCoordinate ggPos)  
        {
            GeoCoordinate bdPos = new GeoCoordinate(ggPos.Latitude, ggPos.Longitude);
            double x = ggPos.Longitude, y = ggPos.Latitude;  
            double z = Math.Sqrt(x * x + y * y) + 0.00002 * Math.Sin(y * x_pi);
            double theta = Math.Atan2(y, x) + 0.000003 * Math.Cos(x * x_pi);
            bdPos.Longitude = z * Math.Cos(theta) + 0.0065;
            bdPos.Latitude = z * Math.Sin(theta) + 0.006;

            return bdPos;
        }

        public static GeoCoordinate BDToMar(GeoCoordinate bdPos)  
        {
            GeoCoordinate ggPos = new GeoCoordinate(bdPos.Latitude, bdPos.Longitude);
            double x = bdPos.Longitude - 0.0065, y = bdPos.Latitude - 0.006;
            double z = Math.Sqrt(x * x + y * y) - 0.00002 * Math.Sin(y * x_pi);
            double theta = Math.Atan2(y, x) - 0.000003 * Math.Cos(x * x_pi);
            ggPos.Longitude = z * Math.Cos(theta);
            ggPos.Latitude = z * Math.Sin(theta);

            return ggPos;
        }

        public static GeoCoordinate BDToGPS(GeoCoordinate bdPos)
        {
            return MarToGPS(BDToMar(bdPos));
        }
        #endregion
    }
}
