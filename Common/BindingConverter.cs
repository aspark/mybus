﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace myBus.Common
{
    public class BooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                var bValue = (bool)value;
                if (parameter != null) bValue = !bValue;
                if (targetType == typeof(Visibility))
                {
                    if (bValue == true)
                        return Visibility.Visible;

                    return Visibility.Collapsed;
                }
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class StringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (targetType == typeof(Visibility))
            {
                var bValue = value != null && !value.ToString().Equals("false", StringComparison.InvariantCultureIgnoreCase);
                if (parameter != null) bValue = !bValue;
                if (bValue == true)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ListConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (targetType == typeof(Visibility))
            {
                if (value is IList)
                {
                    var items = value as IList;
                    var bValue = items.Count > 0;
                    if (parameter != null) bValue = !bValue;
                    if (bValue == true)
                        return Visibility.Visible;
                }

                return Visibility.Collapsed;
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TypeTemplateConvert : FrameworkElement, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return null;

            string typeName = value.GetType().ToString();
            if (parameter != null)
            {
                var property = value.GetType().GetProperty(parameter.ToString(), BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                if (property != null)
                {
                    typeName = property.GetValue(value).ToString();
                }
            }

            var dataTemplate = this.Resources[typeName] as DataTemplate;
            if (dataTemplate == null) return null;

            return dataTemplate;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
