﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Common
{
    class EntityHelper
    {
        public static To Transfer<To>(object fromEntity) where To : new()
        {
            if (fromEntity != null)
            {
                var toEntity = new To();
                var toProperties = toEntity.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (var property in fromEntity.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
                {
                    var prop = toProperties.FirstOrDefault(p => string.Equals(p.Name, property.Name, StringComparison.InvariantCultureIgnoreCase));
                    if (prop != null)
                    {
                        prop.SetValue(toEntity, property.GetValue(fromEntity));
                    }
                }

                return toEntity;
            }

            return default(To);
        }

        public static List<To> Transfer<To>(IEnumerable fromItems) where To : new()
        {
            List<To> toItems = new List<To>();
            if (fromItems != null)
            {
                foreach (var item in fromItems)
                {
                    var to = Transfer<To>(item);
                    if (to != null)
                        toItems.Add(to);
                }
            }

            return toItems;
        }
    }
}
