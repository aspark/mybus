﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace myBus.Common
{
    public class ThicknessAnimationWrapper : FrameworkElement
    {
        public FrameworkElement Target
        {
            get
            {
                return (FrameworkElement)GetValue(TargetProperty);
            }
            set
            {
                SetValue(TargetProperty, value);
            }
        }

        // Using a DependencyProperty as the backing store for Target.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TargetProperty =
            DependencyProperty.Register("Target", typeof(FrameworkElement), typeof(ThicknessAnimationWrapper), new PropertyMetadata(null, OnTargetChanged));


        static void OnTargetChanged(DependencyObject source, DependencyPropertyChangedEventArgs args)
        {
            ThicknessAnimationWrapper sender = (ThicknessAnimationWrapper)source;
            sender.UpdateMargin();
        }




        public String PropertyName
        {
            get
            {
                return (String)GetValue(PropertyNameProperty);
            }
            set
            {
                SetValue(PropertyNameProperty, value);
            }
        }

        // Using a DependencyProperty as the backing store for PropertyName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PropertyNameProperty =
            DependencyProperty.Register("PropertyName", typeof(String), typeof(ThicknessAnimationWrapper), new PropertyMetadata("Margin"));



        public EnumThicknessAnimationWrapperSide Side
        {
            get
            {
                return (EnumThicknessAnimationWrapperSide)GetValue(SideProperty);
            }
            set
            {
                SetValue(SideProperty, value);
            }
        }

        // Using a DependencyProperty as the backing store for Side.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SideProperty =
            DependencyProperty.Register("Side", typeof(EnumThicknessAnimationWrapperSide), typeof(ThicknessAnimationWrapper), new PropertyMetadata(EnumThicknessAnimationWrapperSide.Left, OnSideChanged));


        static void OnSideChanged(DependencyObject source, DependencyPropertyChangedEventArgs args)
        {
            ThicknessAnimationWrapper sender = (ThicknessAnimationWrapper)source;
            sender.UpdateMargin();
        }


        public double Value
        {
            get
            {
                return (double)GetValue(ValueProperty);
            }
            set
            {
                SetValue(ValueProperty, value);
            }
        }

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(ThicknessAnimationWrapper), new PropertyMetadata(0.0, OnValueChanged));


        static void OnValueChanged(DependencyObject source, DependencyPropertyChangedEventArgs args)
        {
            ThicknessAnimationWrapper sender = (ThicknessAnimationWrapper)source;
            sender.UpdateMargin();
        }





        static void OnPropertyChanged(DependencyObject source, DependencyPropertyChangedEventArgs args)
        {
            ThicknessAnimationWrapper sender = (ThicknessAnimationWrapper)source;
            sender.UpdateMargin();
        }

        private void UpdateMargin()
        {
            if (Target != null)
            {
                var thicknessProperty = Target.GetType().GetProperty(PropertyName);
                var currentThickness = (Thickness)thicknessProperty.GetValue(Target, null);
                var nextThickness = new Thickness(
                    CalculateThickness(EnumThicknessAnimationWrapperSide.Left, currentThickness.Left),
                    CalculateThickness(EnumThicknessAnimationWrapperSide.Top, currentThickness.Top),
                    CalculateThickness(EnumThicknessAnimationWrapperSide.Right, currentThickness.Right),
                    CalculateThickness(EnumThicknessAnimationWrapperSide.Bottom, currentThickness.Bottom)
                    );

                thicknessProperty.SetValue(Target, nextThickness, null);
            }
        }

        private double CalculateThickness(EnumThicknessAnimationWrapperSide sideToCalculate, double currentValue)
        {
            return (Side & sideToCalculate) == sideToCalculate ? Value : currentValue;
        }
    }

    [Flags]
    public enum EnumThicknessAnimationWrapperSide
    {
        Left = 1,
        Top = 2,
        Right = 4,
        Bottom = 8,
        All = 15
    }
}
