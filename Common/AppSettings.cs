﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Common
{
    [DataContract]
    public class AppSettings
    {
        private AppSettings()
        {
            IsEnableGeo = true;
            AutoRefreshInterval = 30;
            WaitingAlarmThreshold = 3;
            ArrivedAlarmThreshold = 500;
        }

        private const string _IsoKey = "aspark_AppSettings";
        private static object _settingLock = new object();
        private static AppSettings _current = null;
        public static AppSettings Current
        {
            get {
                if (_current == null)
                {
                    lock (_settingLock)
                    {
                        if (_current == null)
                        {
                            if (IsolatedStorageSettings.ApplicationSettings.Contains(_IsoKey))
                            {
                                _current = IsolatedStorageSettings.ApplicationSettings[_IsoKey] as AppSettings;
                            }
                            else
                            {
                                _current = new AppSettings();
                                IsolatedStorageSettings.ApplicationSettings.Add(_IsoKey, _current);
                                IsolatedStorageSettings.ApplicationSettings.Save();
                            }
                        }
                    }
                }
                return _current;
            }
        }

        [DataMember]
        public bool IsEnableGeo { get; set; }

        [DataMember]
        public bool CanAutoRefresh { get; set; }

        [DataMember]
        public int AutoRefreshInterval { get; set; }//s


        [DataMember]
        public bool NeedWaitingAlarm { get; set; }
        
        [DataMember]
        public int WaitingAlarmThreshold { get; set; }//站或分钟


        [DataMember]
        public bool NeedArrivedAlarm { get; set; }
        
        [DataMember]
        public int ArrivedAlarmThreshold { get; set; }//m

        public void Save() {
            lock (_settingLock)
            {
                if (IsolatedStorageSettings.ApplicationSettings.Contains(_IsoKey))
                {
                    IsolatedStorageSettings.ApplicationSettings[_IsoKey] = this;
                }
                else
                {
                    IsolatedStorageSettings.ApplicationSettings.Add(_IsoKey, this);
                }

                IsolatedStorageSettings.ApplicationSettings.Save();
            }
        }
    }
}
