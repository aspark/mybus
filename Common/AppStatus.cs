﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myBus.Common
{
    class AppStatus
    {
        private AppStatus()
        {

        }

        private static AppStatus _current = new AppStatus();

        internal static AppStatus Current
        {
            get { return AppStatus._current; }
        }

        public string CurrentCity { get; set; }

        /// <summary>
        /// gps
        /// </summary>
        public double CurrentLat { get; set; }

        /// <summary>
        /// gps
        /// </summary>
        public double CurrentLng { get; set; }
    }
}
