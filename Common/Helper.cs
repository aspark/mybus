﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using myBus.Resources;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Windows.Phone.Speech.Synthesis;
using Windows.System.Threading;

namespace myBus.Common
{
    static class Helper
    {

        public static void GetServiceTime(string serviceTime, out DateTime startServiceTime, out DateTime endSerivceTime)
        {
            startServiceTime = DateTime.Now.Date.AddHours(6);
            endSerivceTime = DateTime.Now.Date.AddHours(21);

            if (!string.IsNullOrWhiteSpace(serviceTime))
            {
                var mServiceTime = Regex.Match(serviceTime, @"(?<s>(?<sh>\d{1,2}):(?<sm>\d{1,2})).(?<e>(?<eh>\d{1,2}):(?<em>\d{1,2}))");
                if (mServiceTime.Success)
                {
                    //TimeSpan temp;
                    if (mServiceTime.Groups["s"].Success)
                    {
                        startServiceTime = DateTime.Now.Date.AddHours(int.Parse(mServiceTime.Groups["sh"].Value)).AddMinutes(int.Parse(mServiceTime.Groups["sm"].Value));
                        //if (TimeSpan.TryParseExact(mServiceTime.Groups["s"].Value, @"hh\:mm", CultureInfo.InvariantCulture, out temp))
                        //{
                        //    startServiceTime = DateTime.Now.Date.Add(temp);
                        //}
                    }

                    if (mServiceTime.Groups["e"].Success)
                    {
                        endSerivceTime = DateTime.Now.Date.AddHours(int.Parse(mServiceTime.Groups["eh"].Value)).AddMinutes(int.Parse(mServiceTime.Groups["em"].Value));
                        //if (TimeSpan.TryParseExact(mServiceTime.Groups["e"].Value, @"hh\:mm", CultureInfo.InvariantCulture, out temp))
                        //{
                        //    endSerivceTime = DateTime.Now.Date.Add(temp);
                        //}
                    }
                }
            }

        }

        public static readonly int ArrivedWaitTime = 15;
        public static readonly int ArrivingWaitTime = 40; 

        public static string GetBusServicePrompt(DateTime startServiceTime, DateTime endSerivceTime, string defaultPrompt = "")
        {
            if (DateTime.Now < startServiceTime)
            {
                return "首班车还未发出";//暂停服务或末班车已过站
            }
            else if (endSerivceTime < DateTime.Now)
            {
                return "末班车已过站";
            }

            return defaultPrompt;
        }

        public static string GetRemainTimeString(TimeSpan ts)
        {
            string str = string.Format("约{0}分", Math.Floor(ts.TotalMinutes));
            if (ts.Seconds > 0)
                str += string.Format("{0}秒", ts.Seconds);
            else
                str += "钟";

            return str;
        }

        public static void DisplayPrompt(string content)
        {
            DisplayPrompt(content, TimeSpan.FromSeconds(2));
        }

        public static void DisplayPrompt(string content, TimeSpan duration)
        {
            dynamic container = App.RootFrame;
            while (!(container is Panel))
            {
                if (container.Content != null)
                {
                    container = container.Content;
                }
                else
                    break;
            }

            if (container is Panel)
            {
                DisplayPrompt(container as Panel, content, duration);
            }
        }

        public static void DisplayPrompt(Panel container, string content)
        {
            DisplayPrompt(container, content, TimeSpan.FromSeconds(2));
        }

        public static void DisplayPrompt(Panel container, string content, TimeSpan duration)
        {
            var pop = new Popup();
            pop.IsOpen = true;
            var text = new ContentControl()
            {
                HorizontalAlignment= System.Windows.HorizontalAlignment.Stretch,
                Content = content,
                Foreground=new SolidColorBrush(Colors.White),
                FontWeight= FontWeights.Black,
                Margin = new Thickness(12,6,12,6),
                FontSize=23,
            };

            var backgroundColor = App.Current.Resources["PhoneAccentColor"] != null ? (Color)App.Current.Resources["PhoneAccentColor"] : Color.FromArgb(0xCC, 0x00, 0x6C, 0x19);
            backgroundColor.A = 0xCC;
            var textBG = new StackPanel()
            {
                HorizontalAlignment = System.Windows.HorizontalAlignment.Center,
                VerticalAlignment = System.Windows.VerticalAlignment.Center,
                Margin = new Thickness(0, 230, 0, 0),
                Background = new SolidColorBrush(backgroundColor),
                Opacity = 0
            };
            textBG.Children.Add(text);

            var textContainer = new Grid();
            textContainer.Width = App.Current.Host.Content.ActualWidth;
            textContainer.Height = App.Current.Host.Content.ActualHeight;
            textContainer.Children.Add(textBG);

            pop.Child = textContainer;
            EventHandler handle = null;
            handle = (obj, args) =>
            {
                pop.LayoutUpdated -= handle;
                //textBG.Margin = new Thickness((container.ActualWidth - textBG.ActualWidth) / 2, (container.ActualHeight - textBG.ActualHeight) * 0.75, 0, 0);
                Storyboard sb = new Storyboard();
                DoubleAnimationUsingKeyFrames daFrames = new DoubleAnimationUsingKeyFrames();
                var da = new EasingDoubleKeyFrame();
                da.Value = 1;
                da.KeyTime = KeyTime.FromTimeSpan(new TimeSpan(0, 0, 0, 0, 150));
                daFrames.KeyFrames.Add(da);
                da = new EasingDoubleKeyFrame();
                da.Value = 1;
                da.KeyTime = KeyTime.FromTimeSpan(duration.Subtract(new TimeSpan(0, 0, 0, 0, 300)));
                daFrames.KeyFrames.Add(da);
                da = new EasingDoubleKeyFrame();
                da.Value = 0;
                da.KeyTime = KeyTime.FromTimeSpan(duration);
                daFrames.KeyFrames.Add(da);
                sb.Children.Add(daFrames);
                Storyboard.SetTarget(daFrames, textBG);
                Storyboard.SetTargetProperty(daFrames, new PropertyPath(TextBlock.OpacityProperty));
                sb.Completed += (sender, e) =>
                {
                    container.Children.Remove(textContainer);
                };
                sb.Begin();
            };
            pop.LayoutUpdated += handle;
            container.Children.Add(pop);
            if (container is Grid)
            {
                var grd = container as Grid;
                
                if(grd.ColumnDefinitions.Count>0)
                    Grid.SetColumnSpan(pop, grd.ColumnDefinitions.Count);

                if(grd.RowDefinitions.Count>0)
                    Grid.SetRowSpan(pop, grd.RowDefinitions.Count);
            }
        }

        public static Task PlaySound(string uri, short loopCount = 1)
        {
            return Task.Factory.StartNew(() =>
            {
                if (uri.ToLower().EndsWith("wav"))
                {
                    using (SoundEffect se = SoundEffect.FromStream(Application.GetResourceStream(new Uri(uri, UriKind.RelativeOrAbsolute)).Stream))
                    {
                        do
                        {
                            se.Play(1, 0, 0);
                            Thread.Sleep(se.Duration);
                        }
                        while (--loopCount > 0);
                    }
                }
            });
        }

        public async static void Speech(string text)
        {
            using(SpeechSynthesizer speech = new SpeechSynthesizer())
            {
                await speech.SpeakTextAsync(text);
            }
        }

        public static void LaunchEmail(string content = null, string to = null, string title = null)
        {
            Microsoft.Phone.Tasks.EmailComposeTask task = new Microsoft.Phone.Tasks.EmailComposeTask();
            task.To = "tsui.jacky@gmail.com";
            task.Subject = string.Format("关于 {0}", AppResources.ApplicationTitle);
            task.Body = content ?? "";
            task.Show();

        }

        public static string Default(this string str, string def)
        {
            if (string.IsNullOrWhiteSpace(str))
                return def;

            return str;
        }
    }
}
