﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace myBus
{
    class UrlHelper
    {
        public static string GetUrlQuery(Dictionary<string, object> queries)
        {
            if (queries != null)
                return string.Join("&", queries.Select(p => string.Format("{0}={1}", p.Key, Uri.EscapeUriString((p.Value ?? "").ToString()))));

            return "";
        }

        private static Dictionary<string, object> ConvertToDic(object queries)
        {
            Dictionary<string, object> dicQueries = null;
            if (queries != null)
            {
                dicQueries = new Dictionary<string, object>();
                foreach (var property in RuntimeReflectionExtensions.GetRuntimeProperties(queries.GetType()))
                {
                    dicQueries.Add(property.Name, property.GetValue(queries));
                }
            }

            return dicQueries;
        }

        public static string GetUrlQuery(object queries)
        {
            return GetUrlQuery(ConvertToDic(queries));
        }

        public static string GetUrl(string url, Dictionary<string, object> queries)
        {
            if (!string.IsNullOrWhiteSpace(url) && queries != null)
            {
                if (url.Contains('?'))
                    return url + "&" + GetUrlQuery(queries);
                else
                    return url + "?" + GetUrlQuery(queries);
            }

            return url;
        }

        public static string GetUrl(string url, object queries)
        {
            return GetUrl(url, ConvertToDic(queries));
        }

        public static Uri GetUrl(string url, Dictionary<string, object> queries, UriKind uriKind)
        {
            if (!string.IsNullOrWhiteSpace(url))
            {
                return new Uri(GetUrl(url, queries), uriKind);
            }

            throw new Exception("url:bad format");
        }

        public static Uri GetUrl(string url, object queries, UriKind uriKind)
        {
            return GetUrl(url, ConvertToDic(queries), uriKind);
        }
    }
}
