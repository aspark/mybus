﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using System.Windows.Media.Animation;

namespace myBus.Common
{
    public class MoveTopBehavior : Behavior<FrameworkElement>
    {
        public MoveTopBehavior()
        {

        }

        public double To { get; set; }

        public double From { get; set; }



        public FrameworkElement Target
        {
            get { return (FrameworkElement)GetValue(TargetProperty); }
            set { SetValue(TargetProperty, value); }
        }

        public static readonly DependencyProperty TargetProperty =
            DependencyProperty.Register("Target", typeof(FrameworkElement), typeof(MoveTopBehavior), null);



        public ThicknessAnimationWrapper Wrapper
        {
            get { return (ThicknessAnimationWrapper)GetValue(WrapProperty); }
            set { SetValue(WrapProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Wrap.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WrapProperty =
            DependencyProperty.Register("Wrapper", typeof(ThicknessAnimationWrapper), typeof(MoveTopBehavior), null);


        protected override void OnAttached()
        {
            base.AssociatedObject.Loaded += AssociatedObject_Loaded;
            base.OnAttached();
        }

        void AssociatedObject_Loaded(object sender, RoutedEventArgs e)
        {
            base.AssociatedObject.Loaded -= AssociatedObject_Loaded;
            Initial();
        }

        Storyboard _stbdBegin = null;
        Storyboard _stbdEnd = null;
        private void Initial()
        {
            _stbdBegin = new Storyboard();
            DoubleAnimation aniBegin = new DoubleAnimation();
            aniBegin.Duration = TimeSpan.FromSeconds(0.2);
            Storyboard.SetTarget(aniBegin, Wrapper);
            Storyboard.SetTargetProperty(aniBegin, new PropertyPath(ThicknessAnimationWrapper.ValueProperty));
            aniBegin.To = To;
            _stbdBegin.Children.Add(aniBegin);

            base.AssociatedObject.GotFocus += AssociatedObject_GotFocus;


            _stbdEnd = new Storyboard();
            DoubleAnimation aniEnd = new DoubleAnimation();
            aniEnd.Duration = TimeSpan.FromSeconds(0.1);
            Storyboard.SetTarget(aniEnd, Wrapper);
            Storyboard.SetTargetProperty(aniEnd, new PropertyPath(ThicknessAnimationWrapper.ValueProperty));
            aniEnd.To = From;
            _stbdEnd.Children.Add(aniEnd);
            base.AssociatedObject.LostFocus += AssociatedObject_LostFocus;
        }

        void AssociatedObject_GotFocus(object sender, RoutedEventArgs e)
        {
            if (_stbdBegin != null)
                _stbdBegin.Begin();
        }

        void AssociatedObject_LostFocus(object sender, RoutedEventArgs e)
        {
            if (_stbdEnd != null)
                _stbdEnd.Begin();
        }

        protected override void OnDetaching()
        {
            base.AssociatedObject.GotFocus -= AssociatedObject_GotFocus;
            base.AssociatedObject.LostFocus -= AssociatedObject_LostFocus;
            if (_stbdBegin != null)
            {
                _stbdBegin.Stop();
                _stbdBegin = null;
            }
            if (_stbdEnd != null)
            {
                _stbdEnd.Stop();
                _stbdEnd = null;
            }
            base.OnDetaching();
        }
    }
}
