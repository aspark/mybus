﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace myBus
{
    public static class ExtendMethods
    {
        public static void FillPropertyFromQuery(this PhoneApplicationPage page)
        {
            FillPropertyFromQuery(page, page);
        }
        public static void FillPropertyFromQuery(this PhoneApplicationPage page, object target)
        {
            if (target != null && page.NavigationContext.QueryString != null)
            {
                var pageType = target.GetType();
                foreach (var property in pageType.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => p.GetCustomAttribute(typeof(FillFromQueryAttribute)) != null))
                {
                    if (page.NavigationContext.QueryString.Keys.Contains(property.Name))
                    {
                        property.SetValue(target, Convert.ChangeType(page.NavigationContext.QueryString[property.Name], property.PropertyType));
                    }
                }
            }
        }
    }
}
